import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/authenticate/user_register_login/registerScreen.dart';

import 'package:geek_doctor/home/user_home/welcomeScreen.dart';
import 'package:geek_doctor/AllWidgets/progressDialog.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:geek_doctor/subScreens/resetScreen.dart';
import 'package:overlay_support/overlay_support.dart';

import '../../main.dart';
import '../../services/user_services/auth.dart';
import '../serviceprovider_register_login/loginserviceScreen.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class LoginScreen extends StatefulWidget {
  static const String idScreen = "login";

  

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isHidden = true;

  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  final AuthService _auth = AuthService();
//QuerySnapshot? searchSnapshot;
  // DatabaseService databaseService = new DatabaseService(uid: '');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[400],
        title: Text(
          "Login as Client",
          style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
          textAlign: TextAlign.center,
        ),
      ),
      endDrawer: Container(
        color: Colors.white,
        width: 255.0,
        child: Drawer(
          child: ListView(
            children: [
              // Draw Header
              Container(
                height: 165.0,
                child: DrawerHeader(
                  decoration: BoxDecoration(color: Colors.white),
                  child: Row(
                    children: [
                      // ignore: deprecated_member_use
                      FlatButton.icon(
                          onPressed: () {
                            Navigator.pushNamed(
                                context, LoginServiceScreen.idScreen);
                          },
                          icon: Icon(Icons.home_repair_service_sharp),
                          label: Text(
                            'Login as Service Provider',
                            style:
                            TextStyle(fontSize: 12.0, color: Colors.blue),
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
          child: Column(
            children: [
              SizedBox(
                height: 35.0,
              ),
              Hero(
                tag: 'geek-logo',
                child: Image(
                  image: AssetImage("images/geeklogo.png"),
                  width: 390.0,
                  height: 250.0,
                  alignment: Alignment.center,
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 1.0,
                      ),
                      TextFormField(
                        controller: emailTextEditingController,
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          labelText: "Email",
                          labelStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                        ),
                        style: TextStyle(fontSize: 14.0),

                        validator: (value) {
                        
                      if (value == null || value.isEmpty) {
                          return 'Please enter your email';
                          }
                          return null;
                      },

                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                       TextFormField(
                        controller: passwordTextEditingController,
                        obscureText: _isHidden,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                          labelText: "Password",
                          labelStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                          suffix: InkWell(
                            onTap: () {
                              _isHidden = !_isHidden;
                              //
                              // This is the trick
                              //
                              (context as Element).markNeedsBuild();
                            },
                            child: Icon(
                              _isHidden ? Icons.visibility : Icons.visibility_off,
                            ),
                          ),
                        ),
                        style: TextStyle(fontSize: 14.0),

                        validator: (value) {

                        if (value == null || value.isEmpty) {
                          return 'Please enter your password';
                        }
                          return null;
                        },

                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      // ignore: deprecated_member_use
                      RaisedButton(
                        color: Colors.orange[400],
                        textColor: Colors.white,
                        child: Container(
                          height: 50.0,
                          child: Center(
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                            ),
                          ),
                        ),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(24.0),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Processing Data')),
                  );
                }
                
                          _hasInternet =
                          await InternetConnectionChecker().hasConnection;
                
                          final _color = Colors.red;
                          final text = "No Internet Access";
                
                          if (!_hasInternet) {
                            showSimpleNotification(
                              Text(
                                '$text',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                              background: _color,
                            );
                          } else if (!emailTextEditingController.text
                              .contains("@")) {
                            displayToastMessage(
                                "Email address is not valid.", context);
                          } else if (passwordTextEditingController.text.isEmpty) {
                            displayToastMessage(
                                "Password must be atleast 6 characters.",
                                context);
                          } else {
                            loginAndAuthenticateUser(context);
                          }
                        },
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                            onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => ResetScreen())),
                            child: Text('Forgot Password?'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              // ignore: deprecated_member_use
              FlatButton(
                onPressed: () {
                  Navigator.of(context).push(
                    PageRouteBuilder(
                      transitionDuration: Duration(milliseconds: 1000),
                      pageBuilder: (BuildContext context,
                          Animation<double> animation,
                          Animation<double> secondaryAnimation) {
                        return RegisterScreen();
                      },
                      transitionsBuilder: (BuildContext context,
                          Animation<double> animation,
                          Animation<double> secondaryAnimation,
                          Widget child) {
                        return Align(
                          child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ),
                        );
                      },
                    ),
                  );
                  // Navigator.pushNamed(context, RegisterScreen.idScreen);
                },
                child: RichText(
                  text: TextSpan(
                      text: 'Don\'t have an account?',
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      children: <TextSpan>[
                        TextSpan(
                          text: ' Sign up',
                          style:
                          TextStyle(color: Colors.blueAccent, fontSize: 18),
                          // recognizer: TapGestureRecognizer()
                          //   ..onTap = () {
                          //     // navigate to desired screen
                          //   }
                        )
                      ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void loginAndAuthenticateUser(BuildContext context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ProgressDialog(
            message: "Authenticating, Please wait...",
          );
        });
    dynamic result = await _auth
        .signInWitheEmailAndPassword(
        emailTextEditingController.text, passwordTextEditingController.text)
        .catchError((errMsg) {
      Navigator.pop(context);
      displayToastMessage("Error: " + errMsg.toString(), context);
    });
    /* databaseService
        .getUserByUserEmail(emailTextEditingController.text.trim())
        .then((val) {
      setState(() {
        searchSnapshot = val;
      });
      HelperFunctions.saveUserNameSharedPreference(
          searchSnapshot!.docs[0].get('name') +
              ' ' +
              searchSnapshot!.docs[0].get('last name'));
      print(searchSnapshot!.docs[0].get('name') +
          ' ' +
          searchSnapshot!.docs[0].get('last name'));
    });
*/
    if (result != null) //user HttpStatus.created
        {
      HelperFunctions.saveUserEmailSharedPreference(
          emailTextEditingController.text.trim());
      //  HelperFunctions.saveUserLoggedInSharedPreference(true);

      usersRef.child(result.uid).once().then((DataSnapshot snap) {
        if (snap.value != null) {
          Navigator.pushNamedAndRemoveUntil(
              context, WelcomeScreen.idScreen, (route) => false);

          displayToastMessage("Welcome to GeekDoctor.", context);
        } else {
          Navigator.pop(context);

          _auth.signOut();

          displayToastMessage(
              "No record exists for this user, Please create new account.",
              context);
        }
      });
    } else {
      Navigator.pop(context);

      displayToastMessage("Error Occured.", context);
    }
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message);
}

bool _hasInternet = false;
void checkInternetAvailable() async {
  _hasInternet = await InternetConnectionChecker().hasConnection;

  final _color = _hasInternet ? Colors.green : Colors.red;
  final text = _hasInternet ? " You are Connected" : "No Internet";
  showSimpleNotification(
    Text(
      '$text',
      style: TextStyle(
        color: Colors.white,
        fontSize: 20.0,
      ),
    ),
    background: _color,
  );
}

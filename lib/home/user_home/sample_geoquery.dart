import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/progressDialog.dart';
import 'package:geek_doctor/AllWidgets/textdecoration.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/home/user_home/setAppointment.dart';

import 'package:geoflutterfire2/geoflutterfire2.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// Init firestore and geoFlutterFire
class QueryUserMap extends StatefulWidget {
  static const String idScreen = 'queryuser';

  @override
  _QueryUserMapState createState() => _QueryUserMapState();
}

class _QueryUserMapState extends State<QueryUserMap> {
  final geo = GeoFlutterFire();
  var collectionReference =
      FirebaseFirestore.instance.collection('Service Info');

  Stream<List<DocumentSnapshot<Object?>>>? streamQuery;
  GeoFirePoint center = GeoFirePoint(Constants.lat, Constants.long);
  double radius = 30;
  String field = 'position';

  String name = '';
  String lastname = '';

  String gender = '';
  String address = '';
  String contact = '';
  String imageUrl = '';
  String expertise1 = '';
  String expertise2 = '';
  String expertise3 = '';
  String expertise4 = '';
  String expertise5 = '';
  String email = '';

  showStream() {
    // stream.listen((event) {});
    Stream<List<DocumentSnapshot<Object?>>> stream = geo
        .collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: field, strictMode: true);
    setState(() {
      streamQuery = stream;
    });
  }

  @override
  void initState() {
    showStream();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _showGeekPanel() {
      showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          builder: (context) {
            return FractionallySizedBox(
              heightFactor: 0.9,
              child: Scaffold(
                backgroundColor: Colors.white,
                appBar: AppBar(
                  title: Text('GEEK PROFILE'),
                  backgroundColor: Colors.orange[100],
                ),
                body: SingleChildScrollView(
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10.0,
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(150.0),
                          child: CachedNetworkImage(
                            imageUrl: imageUrl,
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                            height: 250.0,
                            width: 250.0,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Row(
                            children: [],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Column(
                            children: [
                              new Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: new TextFormField(
                                          enabled: false,
                                          initialValue: name,
                                          decoration: textInputDecoration,
                                        ),
                                      ),
                                    ),
                                    new Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: new TextFormField(
                                          enabled: false,
                                          initialValue: lastname,
                                          decoration: textInputDecoration,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 1.0,
                              ),

                              new Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: new TextFormField(
                                          enabled: false,
                                          initialValue: address,
                                          decoration: textInputDecoration,
                                        ),
                                      ),
                                    ),
                                    new Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: new TextFormField(
                                          enabled: false,
                                          initialValue: gender,
                                          decoration: textInputDecoration,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              SizedBox(
                                height: 1.0,
                              ),
                              new TextFormField(
                                enabled: false,
                                initialValue: contact,
                                decoration: textInputDecoration,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text('Expertise:',
                                    style: TextStyle(
                                      fontSize: 16.0,
                                    )),
                              ),
                              SizedBox(
                                height: 1.0,
                              ),
                              TextFormField(
                                enabled: false,
                                initialValue: '1  ' + expertise1,
                                decoration: textInputDecoration,
                              ),
                              SizedBox(
                                height: 1.0,
                              ),
                              TextFormField(
                                enabled: false,
                                initialValue: '2  ' + expertise2,
                                decoration: textInputDecoration,
                              ),
                              SizedBox(
                                height: 1.0,
                              ),
                              TextFormField(
                                enabled: false,
                                initialValue: '3  ' + expertise3,
                                decoration: textInputDecoration,
                              ),
                              SizedBox(
                                height: 1.0,
                              ),
                              TextFormField(
                                enabled: false,
                                initialValue: '4  ' + expertise4,
                                decoration: textInputDecoration,
                              ),
                              SizedBox(
                                height: 1.0,
                              ),
                              TextFormField(
                                enabled: false,
                                initialValue: '5  ' + expertise5,
                                decoration: textInputDecoration,
                              ),
                              SizedBox(
                                height: 30.0,
                              ),
                              RaisedButton(
                                  color: Colors.yellow[200],
                                  textColor: Colors.black,
                                  child: Container(
                                    height: 50.0,
                                    child: Center(
                                      child: Text(
                                        "SET AN APPOINTMENT",
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontFamily: "Brand Bold"),
                                      ),
                                    ),
                                  ),
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(24.0),
                                  ),
                                  onPressed: () async {
                                    Constants.serviceProviderName =
                                        name + ' ' + lastname;
                                    Constants.serviceProviderAddress = address;
                                    Constants.serviceProviderContact = contact;
                                    Constants.serviceProviderImageURL =
                                        imageUrl;
                                    Constants.serviceProviderEmail = email;
                                    Navigator.pushNamed(
                                        context, SetAppointment.idScreen);
                                  }),
                              // ignore: deprecated_member_use
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
    }

    return StreamBuilder<List<DocumentSnapshot>>(
        stream: streamQuery,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final List<DocumentSnapshot<Object?>> documents = snapshot.data!;
            return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: Text('GEEK LIST DATA'),
                backgroundColor: Colors.orange[100],
              ),
              body: ListView(
                  children: documents
                      .map(
                        (doc) => Card(
                          color: Colors.orange[200],
                          margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                          child: ListTile(
                            leading: ClipRRect(
                              borderRadius: BorderRadius.circular(90.0),
                              child: CachedNetworkImage(
                                imageUrl: '${doc['imageURL']}',
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                                height: 60.0,
                                width: 60.0,
                                fit: BoxFit.fill,
                              ),
                            ),
                            title: Text(
                                "'Service Provider: ${doc['name']} ${doc['last name']}'"),
                            subtitle: Text('Reference #:   ${doc.id}  '),
                            onTap: () {
                              name = doc['name'].toString();
                              lastname = doc['last name'].toString();
                              gender = doc['gender'].toString();
                              address = doc['address'].toString();
                              contact = doc['contact number'].toString();
                              expertise1 = doc['expertise1'].toString();
                              expertise2 = doc['expertise2'].toString();
                              expertise3 = doc['expertise3'].toString();
                              expertise4 = doc['expertise4'].toString();
                              expertise5 = doc['expertise5'].toString();
                              imageUrl = doc['imageURL'].toString();
                              email = doc['email'].toString();

                              _showGeekPanel();
                            },
                          ),
                        ),
                      )
                      .toList()),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Scaffold(
            backgroundColor: Colors.white,
            body: ProgressDialog(
              message: " Please wait...",
            ),
          );
        });
  }
}

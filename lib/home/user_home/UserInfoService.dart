import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/AllWidgets/textdecoration.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/models/user_data/user.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:provider/provider.dart';

class UserInfoService extends StatelessWidget {
  static const String idScreen = 'userinfoservice';

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Usercount?>(context);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user!.uid).userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData? userData = snapshot.data;
            return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: Text('GEEK PROFILE'),
                backgroundColor: Colors.orange[100],
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(150.0),
                        child: CachedNetworkImage(
                          imageUrl: userData!.imageURL,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          height: 250.0,
                          width: 250.0,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Row(
                          children: [],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                          children: [
                            new Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: new TextFormField(
                                        enabled: false,
                                        initialValue: userData.name,
                                        decoration: textInputDecoration,
                                      ),
                                    ),
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: new TextFormField(
                                        enabled: false,
                                        initialValue: userData.lastname,
                                        decoration: textInputDecoration,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            new Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: new TextFormField(
                                        enabled: false,
                                        initialValue: userData.birthdate,
                                        decoration: textInputDecoration,
                                      ),
                                    ),
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: new TextFormField(
                                        enabled: false,
                                        initialValue: userData.gender,
                                        decoration: textInputDecoration,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              enabled: false,
                              initialValue: userData.address,
                              decoration: textInputDecoration,
                            ),

                            SizedBox(
                              height: 1.0,
                            ),
                            new TextFormField(
                              enabled: false,
                              initialValue: userData.contact,
                              decoration: textInputDecoration,
                            ),

                            SizedBox(
                              height: 30.0,
                            ),
                            // ignore: deprecated_member_use
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/AllWidgets/textdecoration.dart';
import 'package:geek_doctor/Chatroom/constants.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/models/user_data/user.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:geek_doctor/upload_files/user_Uploads/userfile_Upload.dart';
import 'package:provider/provider.dart';

class UserInfoTile extends StatefulWidget {
  static const String idScreen = 'userinfotile';
  @override
  _UserInfoTileState createState() => _UserInfoTileState();
}

class _UserInfoTileState extends State<UserInfoTile> {
  String _name = '';
  String _lastname = '';
  String _address = '';
  String _contact = '';
  final List<String> genders = [
    'Male',
    'Female',
  ];

  String genderdata = '';
  DateTime? date;
  String birthdate() {
    if (date == null) {
      return '';
    } else {
      return '${date!.month} / ${date!.day} / ${date!.year}';
    }
  }

  @override
  Widget build(BuildContext context) {
    void _showUploadsheet() {
      showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          builder: (context) {
            return FractionallySizedBox(
              heightFactor: 0.5,
              child: UserFileUpload(),
            );
          });
    }

    final user = Provider.of<Usercount?>(context);

    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user!.uid).userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData? userData = snapshot.data;
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    GestureDetector(
                      onTap: () {
                        _showUploadsheet();
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(150.0),
                        child: CachedNetworkImage(
                          imageUrl: userData!.imageURL,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          height: 250.0,
                          width: 250.0,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Row(
                        children: [],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Column(
                        children: [
                          new Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: new TextFormField(
                                      initialValue: userData.name,
                                      decoration: textInputDecoration,
                                      onChanged: (val) =>
                                          setState(() => _name = val),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: new TextFormField(
                                      initialValue: userData.lastname,
                                      decoration: textInputDecoration,
                                      onChanged: (val) =>
                                          setState(() => _lastname = val),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),

                          new Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: RaisedButton(
                                        color: Colors.white,
                                        child: Text(
                                          birthdate() == ''
                                              ? userData.birthdate
                                              : birthdate(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        onPressed: () async {
                                          final initialDate = DateTime.now();
                                          final newDate = await showDatePicker(
                                            context: context,
                                            initialDate: initialDate,
                                            firstDate: DateTime(
                                                DateTime.now().year - 100),
                                            lastDate: DateTime(
                                                DateTime.now().year + 5),
                                          );
                                          if (newDate == null) return;
                                          setState(
                                            () => date = newDate,
                                          );
                                        }),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: DropdownButtonFormField(
                                      decoration: InputDecoration(
                                        labelStyle: TextStyle(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      value: genderdata == ''
                                          ? userData.gender
                                          : userData.gender,
                                      items: genders.map((gender) {
                                        return DropdownMenuItem(
                                          value: gender,
                                          child: Text('$gender'),
                                        );
                                      }).toList(),
                                      onChanged: (val) => setState(
                                          () => genderdata = val.toString()),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: userData.address,
                            decoration: textInputDecoration,
                            onChanged: (val) => setState(() => _address = val),
                          ),

                          SizedBox(
                            height: 1.0,
                          ),
                          new TextFormField(
                            initialValue: userData.contact,
                            decoration: textInputDecoration,
                            onChanged: (val) => setState(() => _contact = val),
                          ),

                          SizedBox(
                            height: 30.0,
                          ),
                          // ignore: deprecated_member_use
                          RaisedButton(
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              child: Container(
                                height: 50.0,
                                child: Center(
                                  child: Text(
                                    "UPDATE",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontFamily: "Brand Bold"),
                                  ),
                                ),
                              ),
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(24.0),
                              ),
                              onPressed: () async {
                                await DatabaseService(uid: user.uid)
                                    .updateUserData(
                                        _name == '' ? userData.name : _name,
                                        _lastname == ''
                                            ? userData.lastname
                                            : _lastname,
                                        birthdate() == ''
                                            ? userData.birthdate
                                            : birthdate(),
                                        genderdata == ''
                                            ? userData.gender
                                            : genderdata,
                                        _address == ''
                                            ? userData.address
                                            : _address,
                                        userData.email,
                                        _contact == ''
                                            ? userData.contact
                                            : _contact,
                                        userData.imageURL,
                                        userData.position);
                                //  Constants.myName ==
                                Navigator.pop(context);
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}

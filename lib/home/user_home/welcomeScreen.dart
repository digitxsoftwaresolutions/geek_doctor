import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/home/service_provider_home/servicemapScreen.dart';

import 'package:geek_doctor/home/user_home/query_file.dart';

import 'package:geek_doctor/AllWidgets/Divider.dart';
import 'package:geek_doctor/home/user_home/sample_geoquery.dart';
import 'package:geek_doctor/home/user_home/userDrawer.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:geek_doctor/subScreens/openingScreen.dart';

import '../../services/user_services/auth.dart';

// ignore: must_be_immutable
class WelcomeScreen extends StatelessWidget {
  static const String idScreen = "welcome";
  final AuthService _auth = AuthService();
  @override
  Widget build(BuildContext context) {
    DatabaseService databaseService = new DatabaseService(uid: '');

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.orange[100],
        title: Padding(
          padding: const EdgeInsets.only(left: 0, right: 0),
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 35,
                  width: 35,
                  child: Image.asset(
                    'images/geeklogo.png',
                    fit: BoxFit.contain,
                    // height: 42,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Geek Doctor',
                    style: TextStyle(color: Colors.orange),
                  ),
                ),
                TextButton.icon(
                    icon: Icon(
                      Icons.search,
                      color: Colors.black,
                      size: 20,
                    ),
                    label: Text(''),
                    onPressed: () {}),
                TextButton.icon(
                    icon: Icon(
                      Icons.logout,
                      color: Colors.black,
                      size: 20,
                    ),
                    label: Text(''),
                    onPressed: () async {
                      _auth.signOut();
                      Navigator.pushNamedAndRemoveUntil(
                          context, OpeningScreen.idScreen, (route) => false);
                    }),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          await databaseService.getUserByEmail(
              (await HelperFunctions.getUserEmailSharedPreference())!);
          Timer(Duration(seconds: 1), () async {
            if (Constants.lat == 0 && Constants.long == 0) {
              print((await HelperFunctions.getUserEmailSharedPreference()));
              displayToastMessage('Please Sign Out and relogin again', context);
            } else {
              print(Constants.lat);
              print(Constants.long);
              // Navigator.pushNamed(context, QueryFile.idScreen);
              Navigator.pushNamed(
                context,
                 QueryFile.idScreen,
              );
            }
          });
        },
        label: const Text(
          'BOOK A GEEK',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        icon: const Icon(
          Icons.home_repair_service_sharp,
          size: 40.0,
        ),
        backgroundColor: Colors.orange,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      drawer: UserDrawer(),
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
        child: Column(
          children: [
            Text(
              "Nothing to show"
              '\n'
              'Demo Purpose Only'
              '\n'
              'Client Main Screen',
            )
          ],
        ),
      )),
    );
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message);
}

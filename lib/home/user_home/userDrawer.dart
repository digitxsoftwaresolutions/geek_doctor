import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/Divider.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/Booking/bookingNotif.dart';
import 'package:geek_doctor/Chatroom/chatRoomScreen.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/Chatroom/search.dart';
import 'package:geek_doctor/History/insideHistory.dart';
import 'package:dart_geohash/dart_geohash.dart';
import 'package:geek_doctor/home/user_home/UserInfoService.dart';
import 'package:geek_doctor/home/user_home/userMapScreen.dart';
import 'package:geek_doctor/home/user_home/userProfile.dart';
import 'package:geek_doctor/models/user_data/user.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:geek_doctor/upload_files/user_Uploads/userfile_Upload.dart';
import 'package:provider/provider.dart';

class UserDrawer extends StatefulWidget {
  const UserDrawer({Key? key}) : super(key: key);

  @override
  _UserDrawerState createState() => _UserDrawerState();
}

class _UserDrawerState extends State<UserDrawer> {
  @override
  Widget build(BuildContext context) {
    // String samplehash = '';
    final user = Provider.of<Usercount?>(context);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user!.uid).userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData? userData = snapshot.data;
            return Container(
              color: Colors.white,
              width: 350.0,
              child: ListView(
                children: [
                  // Draw Header
                  Container(
                    height: 165.0,
                    child: DrawerHeader(
                      decoration: BoxDecoration(color: Colors.white),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, UserFileUpload.idScreen);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(60.0),
                              child: CachedNetworkImage(
                                imageUrl: userData!.imageURL,
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                                height: 75.0,
                                width: 75.0,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: FlatButton(
                                  onPressed: () {},
                                  child: Text(
                                    userData.name + ' ' + userData.lastname,
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontFamily: "Brand-Bold"),
                                  ),
                                ),
                              ),

                              // ignore: deprecated_member_use
                              FlatButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, UserInfoService.idScreen);
                                },
                                child: Text(
                                  'Profile Information',
                                  style: TextStyle(
                                      fontSize: 16.0, fontFamily: "Brand-Bold"),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),

                  DividerWidget(),

                  SizedBox(
                    height: 12.0,
                  ),

                  // Drawer Body Controllers
                  ListTile(
                    leading: Icon(Icons.history),
                    title: Text(
                      "History",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () async {
                      await HelperFunctions.saveUserEmailSharedPreference(
                          userData.email);
                      Navigator.pushNamed(context, History.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text(
                      "Edit Profile",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, UserProfile.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.notifications),
                    title: Text(
                      "Notifications",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () async {
                      await HelperFunctions.saveUserNameSharedPreference(
                        userData.name + ' ' + userData.lastname,
                      );
                      HelperFunctions.saveUserEmailSharedPreference(
                          userData.email);
                      HelperFunctions.saveUserLoggedInSharedPreference(true);
                      Constants.myImageURL = userData.imageURL;
                      Constants.myContactNumber = userData.contact;
                      Navigator.pushNamed(
                          context, BookingNotifications.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.chat, size: 30.0),
                    title: Text(
                      "Messages",
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    onTap: () async {
                      await HelperFunctions.saveUserNameSharedPreference(
                        userData.name + ' ' + userData.lastname,
                      );
                      HelperFunctions.saveUserEmailSharedPreference(
                          userData.email);
                      HelperFunctions.saveUserLoggedInSharedPreference(true);
                      Constants.myImageURL = userData.imageURL;

                      Navigator.pushNamed(context, ChatRoom.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.add_location_sharp),
                    title: Text(
                      "My Current Location",
                      style: TextStyle(
                        fontSize: 12.0,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, UserMapScreen.idScreen);
                    },
                  ),

                  ListTile(
                    leading: Icon(Icons.info),
                    title: Text(
                      "About",
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            );
          } else {
            return Scaffold(
              backgroundColor: Colors.white,
              body: Loading(),
            );
          }
        });
  }
}

import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/AllWidgets/textdecoration.dart';
import 'package:geek_doctor/Booking/book_Database.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/home/user_home/query_file.dart';
import 'package:geek_doctor/home/user_home/welcomeScreen.dart';
import 'package:geek_doctor/models/user_data/user.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:provider/provider.dart';

class SetAppointment extends StatefulWidget {
  static const String idScreen = "setappointment";

  @override
  _SetAppointmentState createState() => _SetAppointmentState();
}

class _SetAppointmentState extends State<SetAppointment> {
  TextEditingController ClientRequestServiceController =
      TextEditingController();
  BookDatabase bookDatabase = new BookDatabase();

  static const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  final List<String> hour = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12'
  ];
  final List<String> minute = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '24',
    '25',
    '26',
    '27',
    '28',
    '29',
    '30',
    '31',
    '32',
    '33',
    '34',
    '35',
    '36',
    '37',
    '38',
    '39',
    '40',
    '41',
    '42',
    '43',
    '44',
    '45',
    '46',
    '47',
    '48',
    '49',
    '50',
    '51',
    '52',
    '53',
    '54',
    '55',
    '56',
    '57',
    '58',
    '59',
    '60'
  ];
  final List<String> amPm = [
    'AM',
    'PM',
  ];

  String hourdata = '';
  String minutedata = '00';
  String ampmData = 'AM';
  DateTime? date;
  String Date() {
    if (date == null) {
      return 'Date';
    } else {
      return '${date!.month} / ${date!.day} / ${date!.year}';
    }
  }

  createBookingInstance(
    String clientName,
    String clientAddress,
    String clientContactNumber,
    String clientImageUrl,
    String clientEmail,
  ) {
    String bookingIdentification = getRandomString(10);
    //  getBookingId(clientEmail, Constants.serviceProviderEmail);
    // String bookingID = getBookingId(clientName, Constants.serviceProviderName);
    List<String> users = [clientEmail, Constants.serviceProviderEmail];
    Map<String, dynamic> bookingMap = {
      'users': users,
      // 'bookingId': bookingID,
      'myImageUrl': clientImageUrl,
      'myAddress': clientAddress,
      'myContactNumber': clientContactNumber,
      'spImageUrl': Constants.serviceProviderImageURL,
      'spAddress': Constants.serviceProviderAddress,
      'spContactNumber': Constants.serviceProviderContact,
      'serviceNeeded': ClientRequestServiceController.text,
      'time': DateTime.now(),
      'bookingStatus': 'Pending',
      'clientName': clientName,
      'spName': Constants.serviceProviderName,
      'scheduleBooking': Date(),
      'schedHour': hourdata,
      'schedMinute': minutedata,
      'schedAmPm': ampmData,
      'spEmail': Constants.serviceProviderEmail,
      'clientEmail': clientEmail,
      'deleteStatus': 'false',
      'clientsHere': 'false',
      'spsHere': 'false',
    };

    BookDatabase().createBooking(bookingIdentification, bookingMap);

    Duration(milliseconds: 2000);
    displayToastMessage(
        "You will see the Service Provider response in Notifications", context);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Usercount?>(context);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user!.uid).userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData? userData = snapshot.data;
            return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: Text('Request Appointment'),
                backgroundColor: Colors.orange[100],
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 30.0, horizontal: 5.0),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(2.0),
                        child: Column(
                          children: [
                            Text('Service Provider Info',
                                style: TextStyle(fontSize: 30.0)),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 80.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Name :',
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: TextFormField(
                                      enabled: false,
                                      initialValue:
                                          Constants.serviceProviderName,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 80.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Address :',
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: TextFormField(
                                      enabled: false,
                                      initialValue:
                                          Constants.serviceProviderAddress,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 120.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Contact Number :',
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: TextFormField(
                                      enabled: false,
                                      initialValue:
                                          Constants.serviceProviderContact,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            Text('Client Info',
                                style: TextStyle(fontSize: 30.0)),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 80.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Name :',
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: TextFormField(
                                      initialValue: userData!.name +
                                          ' ' +
                                          userData.lastname,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 80.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Address :',
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: TextFormField(
                                      initialValue: userData.address,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 120.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Contact Number :',
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: TextFormField(
                                      initialValue: userData.contact,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            Text(
                              'Service Information',
                              style: TextStyle(fontSize: 30.0),
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    width: 120.0,
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 0.0),
                                      child: FittedBox(
                                        fit: BoxFit.scaleDown,
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          'Service Needed :',
                                          style: TextStyle(fontSize: 15.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  new Flexible(
                                    child: TextField(
                                      controller:
                                          ClientRequestServiceController,
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(),
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ]),
                            SizedBox(
                              height: 30.0,
                            ),
                            Text(
                              'Service Schedule',
                              style: TextStyle(fontSize: 30.0),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  RaisedButton(
                                      color: Colors.white,
                                      child: Container(
                                        height: 50.0,
                                        width: 100.0,
                                        child: Center(
                                          child: FittedBox(
                                            fit: BoxFit.scaleDown,
                                            child: Text(
                                              Date(),
                                              style: TextStyle(
                                                  color: Colors.black),
                                            ),
                                          ),
                                        ),
                                      ),
                                      onPressed: () async {
                                        final initialDate = DateTime.now();
                                        final newDate = await showDatePicker(
                                          context: context,
                                          initialDate: initialDate,
                                          firstDate:
                                              DateTime(DateTime.now().year),
                                          lastDate: DateTime(
                                              DateTime.now().year + 10),
                                        );
                                        if (newDate == null) return;
                                        setState(() => date = newDate);
                                      }),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Container(
                                    height: 50.0,
                                    width: 50.0,
                                    child: DropdownButtonFormField(
                                      decoration: InputDecoration(
                                        labelStyle: TextStyle(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      hint: Text(
                                        '0',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontFamily: "Brand Bold"),
                                      ),
                                      items: hour.map((hour) {
                                        return DropdownMenuItem(
                                          value: hour,
                                          child: Text(
                                            '$hour',
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontFamily: "Brand Bold"),
                                          ),
                                        );
                                      }).toList(),
                                      onChanged: (val) => setState(
                                          () => hourdata = val.toString()),
                                    ),
                                  ),
                                  Text(':'),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    height: 50.0,
                                    width: 50.0,
                                    child: DropdownButtonFormField(
                                      decoration: InputDecoration(
                                        labelStyle: TextStyle(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      hint: Text(
                                        '00',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontFamily: "Brand Bold"),
                                      ),
                                      items: minute.map((minute) {
                                        return DropdownMenuItem(
                                          value: minute,
                                          child: Text(
                                            '$minute',
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontFamily: "Brand Bold"),
                                          ),
                                        );
                                      }).toList(),
                                      onChanged: (val) => setState(
                                          () => minutedata = val.toString()),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    height: 50.0,
                                    width: 60.0,
                                    child: DropdownButtonFormField(
                                      decoration: InputDecoration(
                                        labelStyle: TextStyle(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      hint: Text(
                                        'AM',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontFamily: "Brand Bold"),
                                      ),
                                      items: amPm.map((ampm) {
                                        return DropdownMenuItem(
                                          value: ampm,
                                          child: Text(
                                            '$ampm',
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontFamily: "Brand Bold"),
                                          ),
                                        );
                                      }).toList(),
                                      onChanged: (val) => setState(
                                          () => ampmData = val.toString()),
                                    ),
                                  ),
                                ]),
                            SizedBox(
                              height: 50.0,
                            ),
                            RaisedButton(
                                color: Colors.yellow[200],
                                textColor: Colors.black,
                                child: Container(
                                  height: 60.0,
                                  width: 250.0,
                                  child: Center(
                                    child: Text(
                                      "Set Appointment",
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: "Brand Bold"),
                                    ),
                                  ),
                                ),
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                onPressed: () {
                                  if (ClientRequestServiceController
                                      .text.isEmpty) {
                                    displayToastMessage(
                                        "Service Needed is required.", context);
                                  } else if (hourdata == '') {
                                    displayToastMessage(
                                        "Please Input Hour.", context);
                                  } else if (Date() == 'Date') {
                                    displayToastMessage(
                                        "Please Input Date.", context);
                                  } else {
                                    createBookingInstance(
                                        userData.name + ' ' + userData.lastname,
                                        userData.address,
                                        userData.contact,
                                        userData.imageURL,
                                        userData.email);
                                  }
                                }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Scaffold(
              backgroundColor: Colors.white,
              body: Loading(),
            );
          }
        });
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message);
}

/*getBookingId(String a, String b) {
  if (a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
    return '$b\_$a';
  } else {
    return '$a\_$b';
  }
}*/

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/AllWidgets/textdecoration.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:geek_doctor/upload_files/service_Uploads/servicefile_Upload.dart';
import 'package:geek_doctor/upload_files/user_Uploads/userfile_Upload.dart';
import 'package:provider/provider.dart';

class ProfileInfoTile extends StatefulWidget {
  @override
  _ProfileInfoTileState createState() => _ProfileInfoTileState();
}

class _ProfileInfoTileState extends State<ProfileInfoTile> {
  String _name = '';
  String _lastname = '';
  String _address = '';
  String _contact = '';

  String _expertise1 = '';
  String _expertise2 = '';
  String _expertise3 = '';
  String _expertise4 = '';
  String _expertise5 = '';
  final List<String> genders = [
    'Male',
    'Female',
  ];

  String genderdata = '';
  DateTime? date;
  String birthdate() {
    if (date == null) {
      return '';
    } else {
      return '${date!.month} / ${date!.day} / ${date!.year}';
    }
  }

  @override
  Widget build(BuildContext context) {
    void _showUploadsheet() {
      showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          builder: (context) {
            return FractionallySizedBox(
              heightFactor: 0.5,
              child: ServiceFileUpload(),
            );
          });
    }

    final user = Provider.of<Servicecount?>(context);
    return StreamBuilder<ProviderData>(
        stream: ServiceProviderData(uid: user!.uid).providerData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            ProviderData? providerData = snapshot.data;
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    GestureDetector(
                      onTap: () {
                        _showUploadsheet();
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(150.0),
                        child: CachedNetworkImage(
                          imageUrl: providerData!.imageURL,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          height: 250.0,
                          width: 250.0,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Row(
                        children: [],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Column(
                        children: [
                          new Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: new TextFormField(
                                      initialValue: providerData.name,
                                      decoration: textInputDecoration,
                                      onChanged: (val) =>
                                          setState(() => _name = val),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: new TextFormField(
                                      initialValue: providerData.lastname,
                                      decoration: textInputDecoration,
                                      onChanged: (val) =>
                                          setState(() => _lastname = val),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 1.0),
                          new Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: RaisedButton(
                                        color: Colors.white,
                                        child: Text(
                                          birthdate() == ''
                                              ? providerData.birthdate
                                              : birthdate(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        onPressed: () async {
                                          final initialDate = DateTime.now();
                                          final newDate = await showDatePicker(
                                            context: context,
                                            initialDate: initialDate,
                                            firstDate: DateTime(
                                                DateTime.now().year - 100),
                                            lastDate: DateTime(
                                                DateTime.now().year + 5),
                                          );
                                          if (newDate == null) return;
                                          setState(
                                            () => date = newDate,
                                          );
                                        }),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: DropdownButtonFormField(
                                      decoration: InputDecoration(
                                        labelStyle: TextStyle(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      value: genderdata == ''
                                          ? providerData.gender
                                          : providerData.gender,
                                      items: genders.map((gender) {
                                        return DropdownMenuItem(
                                          value: gender,
                                          child: Text('$gender'),
                                        );
                                      }).toList(),
                                      onChanged: (val) => setState(
                                          () => genderdata = val.toString()),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: providerData.address,
                            decoration: textInputDecoration,
                            onChanged: (val) => setState(() => _address = val),
                          ),

                          SizedBox(
                            height: 1.0,
                          ),
                          new TextFormField(
                            initialValue: providerData.contact,
                            decoration: textInputDecoration,
                            onChanged: (val) => setState(() => _contact = val),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text('Expertise:',
                                style: TextStyle(
                                  fontSize: 16.0,
                                )),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: providerData.expertise1,
                            onChanged: (val) =>
                                setState(() => _expertise1 = val),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: providerData.expertise2,
                            onChanged: (val) =>
                                setState(() => _expertise2 = val),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: providerData.expertise3,
                            onChanged: (val) =>
                                setState(() => _expertise3 = val),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: providerData.expertise4,
                            onChanged: (val) =>
                                setState(() => _expertise4 = val),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: providerData.expertise5,
                            onChanged: (val) =>
                                setState(() => _expertise5 = val),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),
                          // ignore: deprecated_member_use
                          RaisedButton(
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              child: Container(
                                height: 50.0,
                                child: Center(
                                  child: Text(
                                    "UPDATE",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontFamily: "Brand Bold"),
                                  ),
                                ),
                              ),
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(24.0),
                              ),
                              onPressed: () async {
                                await ServiceProviderData(uid: user.uid)
                                    .updateServiceData(
                                  _name == '' ? providerData.name : _name,
                                  _lastname == ''
                                      ? providerData.lastname
                                      : _lastname,
                                  birthdate() == ''
                                      ? providerData.birthdate
                                      : birthdate(),
                                  genderdata == ''
                                      ? providerData.gender
                                      : genderdata,
                                  _address == ''
                                      ? providerData.address
                                      : _address,
                                  providerData.email,
                                  _contact == ''
                                      ? providerData.contact
                                      : _contact,
                                  _expertise1 == ''
                                      ? providerData.expertise1
                                      : _expertise1,
                                  _expertise2 == ''
                                      ? providerData.expertise2
                                      : _expertise2,
                                  _expertise3 == ''
                                      ? providerData.expertise3
                                      : _expertise3,
                                  _expertise4 == ''
                                      ? providerData.expertise4
                                      : _expertise4,
                                  _expertise5 == ''
                                      ? providerData.expertise5
                                      : _expertise5,
                                  providerData.imageURL,
                                );

                                Navigator.pop(context);
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}

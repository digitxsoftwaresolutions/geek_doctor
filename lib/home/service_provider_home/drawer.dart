import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/Divider.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/Booking/bookingNotif.dart';
import 'package:geek_doctor/Chatroom/chatRoomScreen.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/History/insideHistory.dart';

import 'package:geek_doctor/home/service_provider_home/profileInfoService.dart';
import 'package:geek_doctor/home/service_provider_home/serviceProviderProfile.dart';
import 'package:geek_doctor/home/service_provider_home/servicemapScreen.dart';
import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:geek_doctor/upload_files/service_Uploads/servicefile_Upload.dart';
import 'package:geek_doctor/upload_files/user_Uploads/userfile_Upload.dart';
import 'package:provider/provider.dart';

class DrawerCount extends StatefulWidget {
  const DrawerCount({Key? key}) : super(key: key);

  @override
  _DrawerCountState createState() => _DrawerCountState();
}

class _DrawerCountState extends State<DrawerCount> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Servicecount?>(context);
    return StreamBuilder<ProviderData>(
        stream: ServiceProviderData(uid: user!.uid).providerData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            ProviderData? providerData = snapshot.data;
            return Container(
              color: Colors.white,
              width: 350.0,
              child: ListView(
                children: [
                  // Draw Header
                  Container(
                    height: 165.0,
                    child: DrawerHeader(
                      decoration: BoxDecoration(color: Colors.white),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, ServiceFileUpload.idScreen);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(60.0),
                              child: CachedNetworkImage(
                                imageUrl: providerData!.imageURL,
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                                height: 75.0,
                                width: 75.0,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // ignore: deprecated_member_use
                              FlatButton(
                                onPressed: () {},
                                child: Text(
                                  providerData.name +
                                      ' ' +
                                      providerData.lastname,
                                  style: TextStyle(
                                      fontSize: 16.0, fontFamily: "Brand-Bold"),
                                ),
                              ),
                              // ignore: deprecated_member_use
                              FlatButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, ProfileInfoService.idScreen);
                                },
                                child: Text(
                                  'Profile Information',
                                  style: TextStyle(
                                      fontSize: 16.0, fontFamily: "Brand-Bold"),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),

                  DividerWidget(),

                  SizedBox(
                    height: 12.0,
                  ),

                  // Drawer Body Controllers
                  ListTile(
                    leading: Icon(Icons.history),
                    title: Text(
                      "History",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () async {
                      await HelperFunctions.saveUserEmailSharedPreference(
                          providerData.email);
                      Navigator.pushNamed(context, History.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text(
                      "Edit Profile",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, ServiceProfile.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.notifications),
                    title: Text(
                      "Notifications",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () async {
                      await HelperFunctions.saveUserNameSharedPreference(
                        providerData.name + ' ' + providerData.lastname,
                      );
                      HelperFunctions.saveUserEmailSharedPreference(
                          providerData.email);
                      HelperFunctions.saveUserLoggedInSharedPreference(true);
                      Constants.myImageURL = providerData.imageURL;
                      Constants.myContactNumber = providerData.contact;
                      Navigator.pushNamed(
                          context, BookingNotifications.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.chat, size: 30.0),
                    title: Text(
                      "Messages",
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    onTap: () async {
                      await HelperFunctions.saveUserNameSharedPreference(
                        providerData.name + ' ' + providerData.lastname,
                      );
                      HelperFunctions.saveUserEmailSharedPreference(
                          providerData.email);
                      HelperFunctions.saveUserLoggedInSharedPreference(true);
                      Constants.myImageURL = providerData.imageURL;
                      Navigator.pushNamed(context, ChatRoom.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.add_location_sharp),
                    title: Text(
                      "My Current Location",
                      style: TextStyle(
                        fontSize: 12.0,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, ServiceMapScreen.idScreen);
                    },
                  ),

                  ListTile(
                    leading: Icon(Icons.info),
                    title: Text(
                      "About",
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}

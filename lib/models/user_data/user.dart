class Usercount {
  final String uid;

  Usercount({required this.uid});
}

class UserData {
  final String uid;
  final String name;
  final String lastname;
  final String birthdate;
  final String gender;
  final String address;
  final String email;
  final String contact;
  final String imageURL;
  final Map position;

  UserData({
    required this.uid,
    required this.name,
    required this.lastname,
    required this.birthdate,
    required this.gender,
    required this.address,
    required this.email,
    required this.contact,
    required this.imageURL,
    required this.position,
  });
}

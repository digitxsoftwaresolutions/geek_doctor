class Servicecount {
  final String uid;

  Servicecount({required this.uid});
}

class ProviderData {
  final String uid;
  final String name;
  final String lastname;
  final String birthdate;
  final String gender;
  final String address;
  final String email;
  final String contact;
  final String expertise1;
  final String expertise2;
  final String expertise3;
  final String expertise4;
  final String expertise5;
  final String imageURL;
  ProviderData({
    required this.uid,
    required this.name,
    required this.lastname,
    required this.birthdate,
    required this.gender,
    required this.address,
    required this.email,
    required this.contact,
    required this.expertise1,
    required this.expertise2,
    required this.expertise3,
    required this.expertise4,
    required this.expertise5,
    required this.imageURL,
  });
}

class BookGeek {
  final String bookgeek;

  BookGeek({required this.bookgeek});
}

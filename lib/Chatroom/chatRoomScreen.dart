import 'dart:ffi';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/Chatroom/chatDatabase.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/Chatroom/conversationScreen.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/Chatroom/search.dart';
import 'package:geek_doctor/home/user_home/query_file.dart';
import 'package:geek_doctor/subScreens/searchScreen.dart';

class ChatRoom extends StatefulWidget {
  static const String idScreen = 'chatroom';

  @override
  _ChatRoomState createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  Stream? chatRoomStream;

  Widget chatRoomList() {
    return StreamBuilder(
        stream: chatRoomStream,
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ListView.builder(
                  itemCount: (snapshot.data! as QuerySnapshot).docs.length,
                  itemBuilder: (context, index) {
                    return ChatRoomTile(
                      userName: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('name'),
                      chatRoomId:
                          (snapshot.data! as QuerySnapshot).docs[index].id,
                      imageUrl: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('imageUrl'),
                      myimageUrl: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('myimageUrl'),
                      email: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('email'),
                      myEmail: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('myEmail'),
                      myName: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('myName'),
                    );
                  })
              : Container();
        });
  }

  @override
  void initState() {
    getUserInfo();

    super.initState();
  }

  getUserInfo() async {
    Constants.myName = (await HelperFunctions.getUserNameSharedPreference())!;
    Constants.myEmail = (await HelperFunctions.getUserEmailSharedPreference())!;
    databaseMethods.getChatRooms(Constants.myEmail).then((value) {
      setState(() {
        chatRoomStream = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat Room'),
        backgroundColor: Colors.orange[100],
      ),
      body: chatRoomList(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        onPressed: () {
          Navigator.pushNamed(context, Search.idScreen);
        },
      ),
    );
  }
}

class ChatRoomTile extends StatelessWidget {
  final String userName;
  final String chatRoomId;
  final String imageUrl;
  final String myimageUrl;
  final String email;
  final String myEmail;
  final String myName;

  ChatRoomTile(
      {required this.userName,
      required this.chatRoomId,
      required this.imageUrl,
      required this.myimageUrl,
      required this.email,
      required this.myEmail,
      required this.myName});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 5.0),
      child: Card(
        color: Colors.orange[200],
        margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
        child: ListTile(
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(90.0),
            child: CachedNetworkImage(
              imageUrl:
                  imageUrl == Constants.myImageURL ? myimageUrl : imageUrl,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: 60.0,
              width: 60.0,
              fit: BoxFit.fill,
            ),
          ),
          title: Text(userName == Constants.myName ? myName : userName,
              style: TextStyle(color: Colors.black, fontSize: 17)),
          subtitle: Text(email == Constants.myEmail ? myEmail : email),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ConversationScreen(chatRoomId)));
          },
        ),
      ),
    );
  }
}

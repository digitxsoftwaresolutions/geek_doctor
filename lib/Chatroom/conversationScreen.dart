import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/Chatroom/chatDatabase.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:flutter/services.dart';

class ConversationScreen extends StatefulWidget {
  String chatRoomId;
  ConversationScreen(this.chatRoomId);

  @override
  _ConversationScreenState createState() => _ConversationScreenState();
}

class _ConversationScreenState extends State<ConversationScreen> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController messageController = new TextEditingController();

  Stream<QuerySnapshot>? chatMessagesStream;
  ScrollController _scrollController = ScrollController();

  _scrollToBottom() {
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }

  Widget ChatMessageList() {
    return StreamBuilder(
        stream: chatMessagesStream,
        builder: (context, snapshot) {
          WidgetsBinding.instance!.addPostFrameCallback((_) {
            if (_scrollController.hasClients) {
              _scrollController
                  .jumpTo(_scrollController.position.maxScrollExtent);
            } else {
              setState(() => null);
            }
          });
          return snapshot.hasData
              ? ListView.builder(
                  shrinkWrap: true,
                  controller: _scrollController,
                  itemCount: (snapshot.data! as QuerySnapshot).docs.length,
                  itemBuilder: (context, index) {
                    return MessageTile(
                        message: (snapshot.data! as QuerySnapshot)
                            .docs[index]
                            .get('message'),
                        isSendByMe: (snapshot.data! as QuerySnapshot)
                                .docs[index]
                                .get('sendby') ==
                            Constants.myEmail);
                  })
              : Container();
        });
  }

  Widget textMessage() {
    return Container(
      alignment: Alignment.bottomCenter,
      // width: MediaQuery.of(context).size.width,

      child: Container(
        color: Color(0x54FFFFFF),
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Row(
          children: [
            Expanded(
              child: TextField(
                onTap: () {
                  Timer(Duration(milliseconds: 500), () => _scrollToBottom());
                },
                controller: messageController,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  hintText: 'Message...',
                  hintStyle: TextStyle(color: Colors.white, fontSize: 16),
                  border: InputBorder.none,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                sendMessage();
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      const Color(0x36FFFFFF),
                      const Color(0x0FFFFFF)
                    ]),
                    borderRadius: BorderRadius.circular(40)),
                padding: EdgeInsets.all(12),
                child: Image.asset(
                  "images/send icon.png",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  sendMessage() {
    if (messageController.text.isNotEmpty) {
      Map<String, dynamic> messageMap = {
        'message': messageController.text,
        'sendby': Constants.myEmail,
        'time': DateTime.now().microsecondsSinceEpoch
      };

      databaseMethods.addConversationMessages(widget.chatRoomId, messageMap);
      // print('sent');
      messageController.text = '';
      Timer(Duration(milliseconds: 500), () => _scrollToBottom());
      /* Timer(
                    Duration(milliseconds: 300),
                    () => SystemChannels.textInput
                        .invokeMethod('TextInput.hide'));
            
              */
    }
  }

  @override
  void initState() {
    _getData();
    _getThingsOnStartup().then((value) {
      _scrollToBottom();
    });

    super.initState();
  }

  Future _getThingsOnStartup() async {
    await Future.delayed(Duration(milliseconds: 1000));
  }

  _getData() async {
    await databaseMethods
        .getConversationMessages(widget.chatRoomId)
        .then((value) {
      setState(() {
        chatMessagesStream = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Conversations'),
        backgroundColor: Colors.orange[100],
      ),
      body: Container(
        color: Colors.black87,
        child: Column(
          children: <Widget>[
            Expanded(child: ChatMessageList()),
            textMessage(),
          ],
        ),
      ),
    );
  }
}

class MessageTile extends StatelessWidget {
  final String message;
  final bool isSendByMe;

  MessageTile({required this.message, required this.isSendByMe});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          left: isSendByMe ? 0 : 24, right: isSendByMe ? 24 : 0),
      margin: EdgeInsets.symmetric(vertical: 8),
      width: MediaQuery.of(context).size.width,
      alignment: isSendByMe ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: isSendByMe
                    ? [const Color(0xff007Ef4), const Color(0xff2A75BC)]
                    : [const Color(0x1AFFFFFF), const Color(0x1AFFFFFF)]),
            borderRadius: isSendByMe
                ? BorderRadius.only(
                    topLeft: Radius.circular(23),
                    topRight: Radius.circular(23),
                    bottomLeft: Radius.circular(23))
                : BorderRadius.only(
                    topLeft: Radius.circular(23),
                    topRight: Radius.circular(23),
                    bottomRight: Radius.circular(23))),
        child:
            Text(message, style: TextStyle(color: Colors.white, fontSize: 16)),
      ),
    );
  }
}

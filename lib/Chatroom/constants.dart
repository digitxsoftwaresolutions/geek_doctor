class Constants {
  static String myName = '';
  static String myEmail = '';
  static String myImageURL = '';
  static String myContactNumber = '';
  static String serviceProviderName = '';
  static String serviceProviderAddress = '';
  static String serviceProviderContact = '';
  static String serviceProviderImageURL = '';
  static String serviceProviderEmail = '';
  static double lat = 0;
  static double long = 0;
  static String chatRoomId = '';
}

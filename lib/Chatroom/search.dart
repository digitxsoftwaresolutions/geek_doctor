import 'dart:async';
import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/Chatroom/chatDatabase.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/Chatroom/conversationScreen.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:geek_doctor/services/user_services/database.dart';

class Search extends StatefulWidget {
  static const String idScreen = 'searchmessage';
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  DatabaseService databaseService = new DatabaseService(uid: '');
  ServiceProviderData serviceProviderData = new ServiceProviderData(uid: '');
  TextEditingController searchTextEditingController = TextEditingController();
  QuerySnapshot? searchSnapshot;
  QuerySnapshot? providerSnapshot;
  QuerySnapshot? chatRoomExist;
  String Userlist = '';
  String providerlist = '';
  String userDat = '';
  String providerDat = '';
  String userResult = '';
  bool Status = false;
  Timer? timer;
  static const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Widget ProviderList() {
    return providerSnapshot != null
        ? ListView.builder(
            itemCount: providerSnapshot!.docs.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return SearchTile(
                username: providerSnapshot!.docs[index].get('name'),
                userLastname: providerSnapshot!.docs[index].get('last name'),
                userEmail: providerSnapshot!.docs[index].get('email'),
                imageUrl: providerSnapshot!.docs[index].get('imageURL'),
              );
            })
        : Container();
  }

  Widget searchList() {
    return searchSnapshot != null
        ? ListView.builder(
            itemCount: searchSnapshot!.docs.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return ProviderTile(
                username: searchSnapshot!.docs[index].get('name'),
                userLastname: searchSnapshot!.docs[index].get('last name'),
                userEmail: searchSnapshot!.docs[index].get('email'),
                imageUrl: searchSnapshot!.docs[index].get('imageURL'),
              );
            })
        : Container();
  }

  @override
  void initState() {
    getUserInfo();

    super.initState();
    timer = Timer.periodic(Duration(milliseconds: 200), (Timer t) => getStat());
  }

  getStat() {
    initiateSearch();
    checkUserExist();
    checkProviderExist();
    if (userDat.isEmpty && providerDat.isEmpty) {
      userResult = 'No result';
    } else {
      userResult = '';
    }
  }

  getUserInfo() async {
    Constants.myName = (await HelperFunctions.getUserNameSharedPreference())!;
    Constants.myEmail = (await HelperFunctions.getUserEmailSharedPreference())!;
  }

  initiateSearch() async {
    if (searchTextEditingController.text.isNotEmpty) {
      setState(() {
        Status = true;
      });
      await databaseService
          .getUserByUsername(searchTextEditingController.text.trim())
          .then((snapshot) {
        searchSnapshot = snapshot;

        setState(() {});
      });
      await serviceProviderData
          .getUserByUsername(searchTextEditingController.text.trim())
          .then((snapshot) {
        providerSnapshot = snapshot;
      });
    } else {
      setState(() {
        Status = false;
      });
    }
  }

  checkChatRoomExist(String chatroomID) async {
    try {
      await databaseMethods.getUserRooms(chatroomID).then((val) {
        setState(() {
          chatRoomExist = val;
        });
        chatRoomExist != null
            ? Constants.chatRoomId = chatRoomExist!.docs[0].id
            : Constants.chatRoomId = '';
      });
    } catch (e) {
      print(e);
      Constants.chatRoomId = '';
    }
  }

  checkUserExist() async {
    try {
      searchSnapshot != null
          ? userDat = searchSnapshot!.docs[0].id
          : userDat = '';
      if (userDat.isNotEmpty) {
        Userlist = 'Users';
      } else {
        Userlist = '';
      }
    } catch (e) {
      print(e);
      userDat = '';
      Userlist = '';
    }
  }

  checkProviderExist() async {
    try {
      providerSnapshot != null
          ? providerDat = providerSnapshot!.docs[0].id
          : providerDat = '';

      if (providerDat.isNotEmpty) {
        providerlist = 'Service Providers';
      } else {
        providerlist = '';
      }
    } catch (e) {
      print(e);
      providerDat = '';
      providerlist = '';
    }
  }

  createChatroomAndStartConversation(
      String userName, String imageUrl, String email) {
    if (email != Constants.myEmail) {
      String chatRoomUsers = getChatroomId(email, Constants.myEmail);
      String chatRoomId = Constants.chatRoomId == ''
          ? getRandomString(10)
          : Constants.chatRoomId;
      List<String> users = [email, Constants.myEmail];
      Map<String, dynamic> chatRoomMap = {
        'users': users,
        'chatroomId': chatRoomUsers,
        'imageUrl': imageUrl,
        'myimageUrl': Constants.myImageURL,
        'email': email,
        'myEmail': Constants.myEmail,
        'myName': Constants.myName,
        'name': userName,
      };

      DatabaseMethods().createChatRoom(chatRoomId, chatRoomMap);
      // Navigator.pushNamed(context, ConversationScreen(chatRoomId).toString());
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ConversationScreen(chatRoomId)));
    } else {
      displayToastMessage("you can't message to yourself", context);
      return;
    }
  }

  Widget SearchTile(
      {String? username,
      String? userLastname,
      String? userEmail,
      String? imageUrl}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(60.0),
            child: CachedNetworkImage(
              imageUrl: imageUrl!,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: 60.0,
              width: 60.0,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(width: 15.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                username! + ' ' + userLastname!,
              ),
              SizedBox(height: 5.0),
              Container(
                height: 20,
                width: 150,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    userEmail!,
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ),
            ],
          ),
          Spacer(),
          GestureDetector(
            onTap: () async {
              await checkChatRoomExist(
                  getChatroomId(userEmail, Constants.myEmail));
              Timer(Duration(milliseconds: 500), () {
                createChatroomAndStartConversation(
                    username + ' ' + userLastname, imageUrl, userEmail);
              });
            },
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(30)),
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Text('Message'),
            ),
          ),
        ],
      ),
    );
  }

  Widget ProviderTile(
      {String? username,
      String? userLastname,
      String? userEmail,
      String? imageUrl}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(60.0),
            child: CachedNetworkImage(
              imageUrl: imageUrl!,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: 60.0,
              width: 60.0,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(width: 15.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                username! + ' ' + userLastname!,
              ),
              SizedBox(height: 5.0),
              Container(
                height: 20,
                width: 150,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    userEmail!,
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ),
            ],
          ),
          Spacer(),
          GestureDetector(
            onTap: () async {
              await checkChatRoomExist(
                  getChatroomId(userEmail, Constants.myEmail));
              Timer(Duration(milliseconds: 500), () {
                createChatroomAndStartConversation(
                    username + ' ' + userLastname, imageUrl, userEmail);
              });
            },
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(30)),
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Text('Message'),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search'),
        backgroundColor: Colors.orange[100],
      ),
      body: SingleChildScrollView(
        child: Scrollbar(
          showTrackOnHover: true,
          child: Column(
            children: [
              Container(
                color: Color(0x54FFFFFF),
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        controller: searchTextEditingController,
                        decoration: InputDecoration(
                          hintText: 'Search Username...',
                          hintStyle: TextStyle(color: Colors.black54),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        initiateSearch();
                      },
                      child: Icon(Icons.search),
                    ),
                  ],
                ),
              ),
              Container(
                child: Status == true
                    ? Column(
                        children: [
                          Center(
                            child: Text(userResult,
                                style: TextStyle(color: Colors.blue)),
                          ),
                          Center(
                            child: Text(Userlist,
                                style: TextStyle(color: Colors.blue)),
                          ),
                          searchList(),
                          Center(
                            child: Text(providerlist,
                                style: TextStyle(color: Colors.blue)),
                          ),
                          ProviderList(),
                        ],
                      )
                    : Container(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message);
}

getChatroomId(String a, String b) {
  if (a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
    return '$b\_$a';
  } else {
    return '$a\_$b';
  }
}

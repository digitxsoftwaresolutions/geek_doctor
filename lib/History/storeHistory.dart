import 'package:geek_doctor/Booking/bookingConstants.dart';
import 'package:geek_doctor/History/historyDatabase.dart';

createHistoryClientNotif() {
  List<String> users = [BookingData.clientEmail];
  Map<String, dynamic> bookingMap = {
    'users': users,
    // 'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'false',
    'clientsHere': 'true',
    'spsHere': 'false',
  };

  HistoryDatabase()
      .createHistory(BookingData.bookingIdentification, bookingMap);
}

createHistorySpNotif() {
  List<String> users = [BookingData.spEmail];
  Map<String, dynamic> bookingMap = {
    'users': users,
    //'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'false',
    'clientsHere': 'false',
    'spsHere': 'true',
  };

  HistoryDatabase()
      .createHistory(BookingData.bookingIdentification, bookingMap);
}

createHistoryAllNotif() {
  List<String> users = [BookingData.clientEmail, BookingData.spEmail];
  Map<String, dynamic> bookingMap = {
    'users': users,
    // 'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'false',
    'clientsHere': 'true',
    'spsHere': 'true',
  };

  HistoryDatabase()
      .createHistory(BookingData.bookingIdentification, bookingMap);
}

createHistoryFixNotif(String Data) {
  List<String> users = [Data];
  Map<String, dynamic> bookingMap = {
    'users': users,
    // 'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'true',
    'clientsHere': 'true',
    'spsHere': 'true',
  };

  HistoryDatabase()
      .createHistory(BookingData.bookingIdentification, bookingMap);
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geek_doctor/Booking/bookingConstants.dart';
import 'package:intl/intl.dart';

class HistoryDatabase {
  createHistory(String historyId, historyMap) {
    FirebaseFirestore.instance
        .collection('History')
        .doc(historyId)
        .set(historyMap)
        .catchError((e) {
      print(e.toString());
    });
  }

  getHistory(String userName) async {
    return await FirebaseFirestore.instance
        .collection('History')
        .where('users', arrayContains: userName)
        .snapshots();
  }

  getHistoryInfo(String historyId) async {
    return await FirebaseFirestore.instance
        .collection('History')
        .doc(historyId)
        .snapshots()
        .listen((event) {
      BookingData.spName = event.get('spName').toString();
      BookingData.spAddress = event.get('spAddress').toString();
      BookingData.spContactNumber = event.get('spContactNumber').toString();
      BookingData.clientName = event.get('clientName').toString();
      BookingData.clientAddress = event.get('myAddress').toString();
      BookingData.clientContactNumber = event.get('myContactNumber').toString();
      BookingData.serviceNeeded = event.get('serviceNeeded').toString();
      BookingData.status = event.get('bookingStatus').toString();
      BookingData.bookingIdentification = historyId;
      BookingData.dateform = event.get('time').toDate();
      BookingData.bookingDateTime =
          DateFormat.yMMMd().add_jm().format(BookingData.dateform).toString();
      BookingData.scheduleBooking = event.get('scheduleBooking').toString();
      BookingData.schedHour = event.get('schedHour').toString();
      BookingData.schedMinute = event.get('schedMinute').toString();
      BookingData.schedAmPm = event.get('schedAmPm').toString();
      // BookingData.bookingId = event.get('bookingId').toString();
      BookingData.clientImageUrl = event.get('myImageUrl').toString();
      BookingData.spImageUrl = event.get('spImageUrl').toString();
      BookingData.clientEmail = event.get('clientEmail').toString();
      BookingData.spEmail = event.get('spEmail').toString();
    });
  }

  deleteHistoryInfo(String historyID) {
    FirebaseFirestore.instance
        .collection('History')
        .doc(historyID)
        .delete()
        .catchError((e) {
      print(e.toString());
    });
  }
}

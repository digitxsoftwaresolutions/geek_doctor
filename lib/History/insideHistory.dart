import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:geek_doctor/Booking/book_Database.dart';
import 'package:geek_doctor/Booking/bookingConstants.dart';
import 'package:geek_doctor/Booking/bookingInfo.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/History/historyDatabase.dart';
import 'package:geek_doctor/History/historyDelete.dart';

import 'historyInfo.dart';

class History extends StatefulWidget {
  static const String idScreen = 'History';

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  HistoryDatabase historyDatabase = new HistoryDatabase();
  Stream<QuerySnapshot>? historyStream;

  Widget bookingList() {
    return StreamBuilder(
        stream: historyStream,
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ListView.builder(
                  itemCount: (snapshot.data! as QuerySnapshot).docs.length,
                  itemBuilder: (context, index) {
                    return BookingTile(
                      spName: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spName'),
                      spEmail: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spEmail'),
                      clientName: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('clientName'),
                      bookingId:
                          (snapshot.data! as QuerySnapshot).docs[index].id,
                      spimageUrl: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spImageUrl'),
                      clientimageUrl: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('myImageUrl'),
                      clientEmail: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('clientEmail'),
                      spcontactnumber: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spContactNumber'),
                      clientcontactnumber: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('myContactNumber'),
                      serviceNeeded: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('serviceNeeded'),
                      currentStatus: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('bookingStatus'),
                      deleteStatus: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('deleteStatus'),
                      clientsHere: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('clientsHere'),
                      spsHere: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spsHere'),
                    );
                  })
              : Container();
        });
  }

  @override
  void initState() {
    getUserInfo();

    super.initState();
  }

  getUserInfo() async {
    Constants.myName = (await HelperFunctions.getUserNameSharedPreference())!;
    Constants.myEmail = (await HelperFunctions.getUserEmailSharedPreference())!;

    historyDatabase.getHistory(Constants.myEmail).then((value) {
      setState(() {
        historyStream = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('History'),
        backgroundColor: Colors.orange[100],
      ),
      body: bookingList(),
    );
  }
}

class BookingTile extends StatelessWidget {
  final String spName;
  final String spEmail;
  final String clientName;
  final String bookingId;
  final String spimageUrl;
  final String clientimageUrl;
  final String clientEmail;
  final String spcontactnumber;
  final String clientcontactnumber;
  final String serviceNeeded;
  final String currentStatus;
  final String deleteStatus;
  final String clientsHere;
  final String spsHere;
  BookingTile(
      {required this.spName,
      required this.spEmail,
      required this.clientName,
      required this.bookingId,
      required this.spimageUrl,
      required this.clientimageUrl,
      required this.clientEmail,
      required this.spcontactnumber,
      required this.clientcontactnumber,
      required this.serviceNeeded,
      required this.currentStatus,
      required this.deleteStatus,
      required this.clientsHere,
      required this.spsHere});
  HistoryDatabase historyDatabase = new HistoryDatabase();

  @override
  Widget build(BuildContext context) {
    deleteHistoryInfo(String bookingID) {
      historyDatabase.deleteHistoryInfo(bookingID);
    }

    return Padding(
      padding: EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: ((MediaQuery.of(context).size.width) -
                    (MediaQuery.of(context).size.width * 0.3)),
                height: 115,
                decoration: BoxDecoration(
                  color: Colors.orange[200],
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(60.0),
                      child: CachedNetworkImage(
                        imageUrl: spEmail == Constants.myEmail
                            ? clientimageUrl
                            : spimageUrl,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: 75.0,
                        width: 75.0,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      height: 115,
                      width: ((MediaQuery.of(context).size.width) -
                          (MediaQuery.of(context).size.width * 0.70)),
                      // color: Colors.brown,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            // color: Colors.white,
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            height: 27,
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                spEmail == Constants.myEmail
                                    ? clientName
                                    : spName,
                                style: TextStyle(
                                    fontSize: 16.0, fontFamily: "Brand-Bold"),
                              ),
                            ),
                          ),
                          Container(
                            // color: Colors.white,
                            height: 27,
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                spEmail == Constants.myEmail
                                    ? clientcontactnumber
                                    : spcontactnumber,
                                style: TextStyle(
                                    fontSize: 16.0, fontFamily: "Brand-Bold"),
                              ),
                            ),
                          ),
                          Container(
                            // color: Colors.white,
                            height: 27,
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                'Status:  ' + currentStatus,
                                style: TextStyle(
                                    fontSize: 12.0, fontFamily: "Brand-Bold"),
                              ),
                            ),
                          ),
                          Container(
                            height: 30,
                            color: Colors.blue[50],
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            child: GestureDetector(
                              onTap: () async {
                                await historyDatabase.getHistoryInfo(bookingId);

                                Timer(Duration(seconds: 1), () {
                                  Navigator.pushNamed(
                                      context, HistoryInfo.idScreen);
                                });
                              },
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Center(
                                  child: Text(
                                    'Click here for more Info...',
                                    style: TextStyle(
                                        fontSize: 10.0,
                                        fontFamily: "Brand-Bold",
                                        color: Colors.blue),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 2,
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.orange[200],
                  borderRadius: BorderRadius.circular(5.0),
                ),
                width: (MediaQuery.of(context).size.width) -
                    (MediaQuery.of(context).size.width * 0.74),
                height: 115,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: GestureDetector(
                    onTap: () async {
                      await historyDatabase.getHistoryInfo(bookingId);
                      Timer(Duration(seconds: 1), () {
                        if (spEmail == Constants.myEmail) {
                          if (deleteStatus == 'false' &&
                              clientsHere == 'false') {
                            //  displayToastMessage('good', context);
                            deleteHistoryAllInstance('false', 'true');
                            updateBookingFixInstance(
                                BookingData.clientEmail, 'false', 'true');
                          } else if (deleteStatus == 'false' &&
                              clientsHere == 'true') {
                            // displayToastMessage('good1', context);
                            deleteHistorySpInstance();
                          } else if (deleteStatus == 'true' &&
                              clientsHere == 'true' &&
                              spsHere == 'true') {
                            //  displayToastMessage('good2', context);
                            deleteHistoryInfo(bookingId);
                          }
                        } else {
                          if (deleteStatus == 'false' && spsHere == 'false') {
                            //   displayToastMessage('good', context);
                            deleteHistoryAllInstance('true', 'false');
                            updateBookingFixInstance(
                                BookingData.spEmail, 'true', 'false');
                          } else if (deleteStatus == 'false' &&
                              spsHere == 'true') {
                            // displayToastMessage('good1', context);
                            deleteHistoryClientInstance();
                          } else if (deleteStatus == 'true' &&
                              clientsHere == 'true' &&
                              spsHere == 'true') {
                            //   displayToastMessage('good2', context);
                            deleteHistoryInfo(bookingId);
                          }
                        }
                      });
                    },
                    child: Text(
                      'Delete',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                ),
              ),
            ]),
      ),
    );
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_LONG);
}

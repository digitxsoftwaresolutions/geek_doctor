import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geek_doctor/Booking/book_Database.dart';
import 'package:geek_doctor/Booking/bookingConstants.dart';

class HistoryInfo extends StatefulWidget {
  static const String idScreen = 'historyinfo';

  @override
  _HistoryInfoState createState() => _HistoryInfoState();
}

class _HistoryInfoState extends State<HistoryInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('History Info'),
        backgroundColor: Colors.orange[100],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 5.0),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(2.0),
                child: Column(
                  children: [
                    Text('Service Provider Info',
                        style: TextStyle(fontSize: 30.0)),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Name :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.spName,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Address :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.spAddress,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 120.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Contact Number :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.spContactNumber,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text('Client Info', style: TextStyle(fontSize: 30.0)),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Name :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.clientName,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Address :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.clientAddress,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 120.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Contact Number :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.clientContactNumber,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text(
                      'Service Needed',
                      style: TextStyle(fontSize: 30.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    TextFormField(
                      initialValue: BookingData.serviceNeeded,
                      style: TextStyle(fontSize: 15.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 140.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Booking Created :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.bookingDateTime,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 120.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Service Schedule :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.scheduleBooking +
                                  ' ' +
                                  BookingData.schedHour +
                                  ':' +
                                  BookingData.schedMinute +
                                  ' ' +
                                  BookingData.schedAmPm,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 120.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Booking Status :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.status,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

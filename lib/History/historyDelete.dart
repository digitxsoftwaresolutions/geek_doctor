import 'package:geek_doctor/Booking/book_Database.dart';
import 'package:geek_doctor/Booking/bookingConstants.dart';
import 'package:geek_doctor/History/historyDatabase.dart';

deleteHistoryClientInstance() async {
  List<String> users = [BookingData.spEmail];
  Map<String, dynamic> bookingMap = {
    'users': users,
    //'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'true',
    'clientsHere': 'true',
    'spsHere': 'true',
  };

  HistoryDatabase()
      .createHistory(BookingData.bookingIdentification, bookingMap);
}

deleteHistorySpInstance() async {
  List<String> users = [BookingData.clientEmail];
  Map<String, dynamic> bookingMap = {
    'users': users,
    // 'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'true',
    'clientsHere': 'true',
    'spsHere': 'true',
  };

  HistoryDatabase()
      .createHistory(BookingData.bookingIdentification, bookingMap);
}

deleteHistoryAllInstance(String Data1, String Data2) async {
  List<String> users = [''];
  Map<String, dynamic> bookingMap = {
    'users': users,
    // 'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'true',
    'clientsHere': Data1,
    'spsHere': Data2,
  };

  HistoryDatabase()
      .createHistory(BookingData.bookingIdentification, bookingMap);
}

updateBookingFixInstance(String Data1, String Data2, String Data3) {
  List<String> users = [Data1];
  Map<String, dynamic> bookingMap = {
    'users': users,
    // 'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'true',
    'clientsHere': Data2,
    'spsHere': Data3,
  };

  BookDatabase().createBooking(BookingData.bookingIdentification, bookingMap);
  //print(BookingData.status);
}

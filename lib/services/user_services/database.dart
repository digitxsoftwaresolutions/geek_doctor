import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_geohash/dart_geohash.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/models/user_data/user.dart';

class DatabaseService {
  final String uid;
  DatabaseService({required this.uid});
//final  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  //collection reference

  final CollectionReference geekCollection =
      FirebaseFirestore.instance.collection('Client Info');

  get value => null;

  Future updateUserData(
    String name,
    String lastname,
    String birthdate,
    String gender,
    String address,
    String email,
    String contact,
    String imageURL,
    Map position,
  ) async {
    return await geekCollection.doc(uid).set({
      'name': name,
      'last name': lastname,
      'birthdate': birthdate,
      'gender': gender,
      'address': address,
      'email': email,
      'contact number': contact,
      'imageURL': imageURL,
      'position': position
    });
  }

  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
      uid: uid,
      name: (snapshot.get('name')),
      lastname: (snapshot.get('last name')),
      birthdate: (snapshot.get('birthdate')),
      gender: (snapshot.get('gender')),
      address: (snapshot.get('address')),
      email: (snapshot.get('email')),
      contact: (snapshot.get('contact number')),
      imageURL: (snapshot.get('imageURL')),
      position: (snapshot.get('position')),
    );
  }

  Stream<UserData> get userData {
    return geekCollection.doc(uid).snapshots().map(_userDataFromSnapshot);
  }

  getUserByUsername(String username) async {
    return await geekCollection
        .where('name', isGreaterThanOrEqualTo: username)
        .where('name', isLessThanOrEqualTo: username + '\uf8ff')
        .get();
  }

  getUserByUserEmail(String userEmail) async {
    return await geekCollection.where('email', isEqualTo: userEmail).get();
  }

  getUserByEmail(String userEmail) async {
    await geekCollection
        .where('email', isEqualTo: userEmail)
        .snapshots()
        .listen((event) async {
      var myHash = GeoHash(event.docs[0]
          .get('position')
          .toString()
          .replaceAll('{', '')
          .replaceAll('}', '')
          .replaceAll('geohash:', '')
          .replaceAll("geopoint: Instance of 'GeoPoint'", '')
          .replaceAll(',', '')
          .toString()
          .trim());
      Constants.lat = myHash.latitude(decimalAccuracy: 5);
      Constants.long = myHash.longitude(decimalAccuracy: 5);
    });
  }
}

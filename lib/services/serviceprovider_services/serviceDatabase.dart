import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';

class ServiceProviderData {
  final String uid;
  ServiceProviderData({required this.uid});

  //collection reference

  final CollectionReference geekCollection =
      FirebaseFirestore.instance.collection('Service Info');

  get value => null;

  Future updateServiceData(
    String name,
    String lastname,
    String birthdate,
    String gender,
    String address,
    String email,
    String contact,
    String expertise1,
    String expertise2,
    String expertise3,
    String expertise4,
    String expertise5,
    String imageURL,
  ) async {
    return await geekCollection.doc(uid).set({
      'name': name,
      'last name': lastname,
      'birthdate': birthdate,
      'gender': gender,
      'address': address,
      'email': email,
      'contact number': contact,
      'expertise1': expertise1,
      'expertise2': expertise2,
      'expertise3': expertise3,
      'expertise4': expertise4,
      'expertise5': expertise5,
      'imageURL': imageURL,
    });
  }

  ProviderData _providerDataFromSnapshot(DocumentSnapshot snapshot) {
    return ProviderData(
      uid: uid,
      name: (snapshot.get('name')),
      lastname: (snapshot.get('last name')),
      birthdate: (snapshot.get('birthdate')),
      gender: (snapshot.get('gender')),
      address: (snapshot.get('address')),
      email: (snapshot.get('email')),
      contact: (snapshot.get('contact number')),
      expertise1: (snapshot.get('expertise1')),
      expertise2: (snapshot.get('expertise2')),
      expertise3: (snapshot.get('expertise3')),
      expertise4: (snapshot.get('expertise4')),
      expertise5: (snapshot.get('expertise5')),
      imageURL: (snapshot.get('imageURL')),
    );
  }

  Stream<ProviderData> get providerData {
    return geekCollection.doc(uid).snapshots().map(_providerDataFromSnapshot);
  }

  getUserByUsername(String username) async {
    return await geekCollection
        .where('name', isGreaterThanOrEqualTo: username)
        .where('name', isLessThanOrEqualTo: username + '\uf8ff')
        .get();
  }

  getUserByUserEmail(String userEmail) async {
    return await geekCollection.where('email', isEqualTo: userEmail).get();
  }
}

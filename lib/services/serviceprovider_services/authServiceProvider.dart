import 'package:firebase_auth/firebase_auth.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';

class AuthServiceProvider {
  final FirebaseAuth _auth = FirebaseAuth.instance;

// create user obj based on firebaseUser

  Servicecount? _userFromFirebaseUser(User? user) {
    return user != null ? Servicecount(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<Servicecount?> get user {
    return _auth.authStateChanges().map(_userFromFirebaseUser);
  }

  // sign in with email & passowrd
  Future signInWitheEmailAndPasswordServiceProvider(
      String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User? user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // register with email & password user
  Future registerWitheEmailAndPasswordServiceProvider(
      String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User? user = result.user;
      //create a new document for the user with the uid

      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign out
  Future signOutServiceProvider() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future resetPass(String email) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    } catch (e) {
      print(e.toString());
    }
  }
}

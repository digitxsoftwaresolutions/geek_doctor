import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'package:geek_doctor/models/user_data/user.dart';

import 'package:geek_doctor/home/user_home/welcomeScreen.dart';
import 'package:geek_doctor/wrap_verify/wrap2.dart';
import 'package:geek_doctor/AllWidgets/progressDialog.dart';
import 'package:geek_doctor/main.dart';
import 'package:provider/provider.dart';

class Verify extends StatefulWidget {
  static const String idScreen = 'verify';

  @override
  _VerifyState createState() => _VerifyState();

// Here you can write your code
}

class _VerifyState extends State<Verify> {
  @override
  void initState() {
    _getThingsOnStartup().then((value) {
      _getvalueUser.call();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: ProgressDialog(
          message: "Authenticating, Please wait...",
        ));
  }

  Future _getThingsOnStartup() async {
    await Future.delayed(Duration(seconds: 1));
  }

  void _getvalueUser() {
    final user = Provider.of<Usercount?>(context, listen: false);

    usersRef.child(user!.uid).once().then((DataSnapshot snap) {
      if (snap.value != null) {
        Navigator.pushNamedAndRemoveUntil(
            context, WelcomeScreen.idScreen, (route) => false);
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, Wrapper2.idScreen, (route) => false);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}

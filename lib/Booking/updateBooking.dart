import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/Booking/book_Database.dart';
import 'package:geek_doctor/Booking/bookingConstants.dart';
import 'package:geek_doctor/Booking/bookingNotif.dart';

class UpdateBooking extends StatefulWidget {
  static const String idScreen = 'updatebooking';

  @override
  _UpdateBookingState createState() => _UpdateBookingState();
}

class _UpdateBookingState extends State<UpdateBooking> {
  String _clientName = '';
  String _clientAddress = '';
  String _clientContactNumber = '';
  String _serviceNeeded = '';

  final List<String> hour = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12'
  ];
  final List<String> minute = [
    '00',
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '24',
    '25',
    '26',
    '27',
    '28',
    '29',
    '30',
    '31',
    '32',
    '33',
    '34',
    '35',
    '36',
    '37',
    '38',
    '39',
    '40',
    '41',
    '42',
    '43',
    '44',
    '45',
    '46',
    '47',
    '48',
    '49',
    '50',
    '51',
    '52',
    '53',
    '54',
    '55',
    '56',
    '57',
    '58',
    '59',
    '60',
  ];
  final List<String> amPm = [
    'AM',
    'PM',
  ];

  String hourdata = '';
  String minutedata = '';
  String ampmData = '';
  DateTime? date;
  String Date() {
    if (date == null) {
      return '';
    } else {
      return '${date!.month} / ${date!.day} / ${date!.year}';
    }
  }

  createBookingInstance(
    String clientName,
    String clientAddress,
    String clientContactNumber,
    String serviceNeeded,
  ) {
    List<String> users = [BookingData.clientEmail, BookingData.spEmail];
    Map<String, dynamic> bookingMap = {
      'users': users,
      // 'bookingId': BookingData.bookingId,
      'myImageUrl': BookingData.clientImageUrl,
      'myAddress': clientAddress,
      'myContactNumber': clientContactNumber,
      'spImageUrl': BookingData.spImageUrl,
      'spAddress': BookingData.spAddress,
      'spContactNumber': BookingData.spContactNumber,
      'serviceNeeded': serviceNeeded,
      'time': BookingData.dateform,
      'bookingStatus': 'Pending',
      'clientName': clientName,
      'spName': BookingData.spName,
      'scheduleBooking': Date() == '' ? BookingData.scheduleBooking : Date(),
      'schedHour': hourdata == '' ? BookingData.schedHour : hourdata,
      'schedMinute': minutedata == '' ? BookingData.schedMinute : minutedata,
      'schedAmPm': ampmData == '' ? BookingData.schedAmPm : ampmData,
      'spEmail': BookingData.spEmail,
      'clientEmail': BookingData.clientEmail,
      'deleteStatus': 'false',
      'clientsHere': 'false',
      'spsHere': 'false',
    };

    BookDatabase().createBooking(BookingData.bookingIdentification, bookingMap);

    Duration(milliseconds: 2000);
    displayToastMessage("Updated", context);

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Update Booking Info'),
        backgroundColor: Colors.orange[100],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 5.0),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(2.0),
                child: Column(
                  children: [
                    Text('Service Provider Info',
                        style: TextStyle(fontSize: 30.0)),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Name :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.spName,
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Address :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.spAddress,
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 120.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Contact Number :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              enabled: false,
                              initialValue: BookingData.spContactNumber,
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text('Client Info', style: TextStyle(fontSize: 30.0)),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Name :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              initialValue: BookingData.clientName,
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                              onChanged: (val) =>
                                  setState(() => _clientName = val),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 80.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Address :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              initialValue: BookingData.clientAddress,
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                              onChanged: (val) =>
                                  setState(() => _clientAddress = val),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 120.0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Contact Number :',
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                        ),
                        new Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: TextFormField(
                              initialValue: BookingData.clientContactNumber,
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                              onChanged: (val) =>
                                  setState(() => _clientContactNumber = val),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text(
                      'Service Information',
                      style: TextStyle(fontSize: 30.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        width: 120.0,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0.0),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Service Needed :',
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ),
                      new Flexible(
                        child: TextFormField(
                          initialValue: BookingData.serviceNeeded,
                          style: TextStyle(
                              fontSize: 18.0, fontFamily: "Brand Bold"),
                          onChanged: (val) =>
                              setState(() => _serviceNeeded = val),
                        ),
                      ),
                    ]),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        width: 160.0,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0.0),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Appointment Created :',
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ),
                      new Flexible(
                        child: TextFormField(
                          enabled: false,
                          initialValue: BookingData.bookingDateTime,
                          style: TextStyle(
                              fontSize: 18.0, fontFamily: "Brand Bold"),
                        ),
                      ),
                    ]),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text(
                      'Service Schedule',
                      style: TextStyle(fontSize: 30.0),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      RaisedButton(
                          color: Colors.white,
                          child: Container(
                            height: 50.0,
                            width: 100.0,
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Center(
                                child: Text(
                                  Date() == ''
                                      ? BookingData.scheduleBooking
                                      : Date(),
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ),
                          ),
                          onPressed: () async {
                            final initialDate = DateTime.now();
                            final newDate = await showDatePicker(
                              context: context,
                              initialDate: initialDate,
                              firstDate: DateTime(DateTime.now().year),
                              lastDate: DateTime(DateTime.now().year + 10),
                            );
                            if (newDate == null) return;
                            setState(() => date = newDate);
                          }),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        height: 50.0,
                        width: 50.0,
                        child: DropdownButtonFormField(
                          decoration: InputDecoration(
                            labelStyle: TextStyle(
                              fontSize: 14.0,
                            ),
                          ),
                          value: hourdata == ''
                              ? BookingData.schedHour
                              : BookingData.schedHour,
                          items: hour.map((hour) {
                            return DropdownMenuItem(
                              value: hour,
                              child: Text(
                                '$hour',
                                style: TextStyle(
                                    fontSize: 18.0, fontFamily: "Brand Bold"),
                              ),
                            );
                          }).toList(),
                          onChanged: (val) =>
                              setState(() => hourdata = val.toString()),
                        ),
                      ),
                      Text(':'),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 50.0,
                        width: 50.0,
                        child: DropdownButtonFormField(
                          decoration: InputDecoration(
                            labelStyle: TextStyle(
                              fontSize: 14.0,
                            ),
                          ),
                          value: minutedata == ''
                              ? BookingData.schedMinute
                              : BookingData.schedMinute,
                          items: minute.map((minute) {
                            return DropdownMenuItem(
                              value: minute,
                              child: Text(
                                '$minute',
                                style: TextStyle(
                                    fontSize: 18.0, fontFamily: "Brand Bold"),
                              ),
                            );
                          }).toList(),
                          onChanged: (val) =>
                              setState(() => minutedata = val.toString()),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 50.0,
                        width: 60.0,
                        child: DropdownButtonFormField(
                          decoration: InputDecoration(
                            labelStyle: TextStyle(
                              fontSize: 14.0,
                            ),
                          ),
                          value: ampmData == ''
                              ? BookingData.schedAmPm
                              : BookingData.schedAmPm,
                          items: amPm.map((ampm) {
                            return DropdownMenuItem(
                              value: ampm,
                              child: Text(
                                '$ampm',
                                style: TextStyle(
                                    fontSize: 18.0, fontFamily: "Brand Bold"),
                              ),
                            );
                          }).toList(),
                          onChanged: (val) =>
                              setState(() => ampmData = val.toString()),
                        ),
                      ),
                    ]),
                    SizedBox(
                      height: 50.0,
                    ),
                    RaisedButton(
                        color: Colors.yellow[200],
                        textColor: Colors.black,
                        child: Container(
                          height: 60.0,
                          width: 250.0,
                          child: Center(
                            child: Text(
                              "Update Appointment",
                              style: TextStyle(
                                  fontSize: 18.0, fontFamily: "Brand Bold"),
                            ),
                          ),
                        ),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                        onPressed: () {
                          createBookingInstance(
                              _clientName == ''
                                  ? BookingData.clientName
                                  : _clientName,
                              _clientAddress == ''
                                  ? BookingData.clientAddress
                                  : _clientAddress,
                              _clientContactNumber == ''
                                  ? BookingData.clientContactNumber
                                  : _clientContactNumber,
                              _serviceNeeded == ''
                                  ? BookingData.serviceNeeded
                                  : _serviceNeeded);
                        }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message);
}

import 'package:geek_doctor/Booking/book_Database.dart';
import 'package:geek_doctor/Booking/bookingConstants.dart';

updateBookingInstance() {
  List<String> users = [BookingData.clientEmail, BookingData.spEmail];
  Map<String, dynamic> bookingMap = {
    'users': users,
    // 'bookingId': BookingData.bookingId,
    'myImageUrl': BookingData.clientImageUrl,
    'myAddress': BookingData.clientAddress,
    'myContactNumber': BookingData.clientContactNumber,
    'spImageUrl': BookingData.spImageUrl,
    'spAddress': BookingData.spAddress,
    'spContactNumber': BookingData.spContactNumber,
    'serviceNeeded': BookingData.serviceNeeded,
    'time': BookingData.dateform,
    'bookingStatus': BookingData.status,
    'clientName': BookingData.clientName,
    'spName': BookingData.spName,
    'scheduleBooking': BookingData.scheduleBooking,
    'schedHour': BookingData.schedHour,
    'schedMinute': BookingData.schedMinute,
    'schedAmPm': BookingData.schedAmPm,
    'spEmail': BookingData.spEmail,
    'clientEmail': BookingData.clientEmail,
    'deleteStatus': 'false',
    'clientsHere': 'false',
    'spsHere': 'false',
  };

  BookDatabase().createBooking(BookingData.bookingIdentification, bookingMap);
  //print(BookingData.status);
}

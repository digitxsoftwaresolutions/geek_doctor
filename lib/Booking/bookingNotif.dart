import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/Booking/BookingUpdate.dart';

import 'package:geek_doctor/Booking/book_Database.dart';
import 'package:geek_doctor/Booking/bookingConstants.dart';
import 'package:geek_doctor/Booking/bookingInfo.dart';
import 'package:geek_doctor/Booking/updateBooking.dart';
import 'package:geek_doctor/Chatroom/constants.dart';
import 'package:geek_doctor/Chatroom/helperFunctions.dart';
import 'package:geek_doctor/History/storeHistory.dart';

import 'bookingNotifDelete.dart';

class BookingNotifications extends StatefulWidget {
  static const String idScreen = 'bookingnotifications';

  @override
  _BookingNotificationsState createState() => _BookingNotificationsState();
}

class _BookingNotificationsState extends State<BookingNotifications> {
  BookDatabase bookDatabase = new BookDatabase();
  Stream<QuerySnapshot>? bookingStream;

  Widget bookingList() {
    return StreamBuilder(
        stream: bookingStream,
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ListView.builder(
                  itemCount: (snapshot.data! as QuerySnapshot).docs.length,
                  itemBuilder: (context, index) {
                    return BookingTile(
                      spName: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spName'),
                      spEmail: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spEmail'),
                      clientName: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('clientName'),
                      bookingId:
                          (snapshot.data! as QuerySnapshot).docs[index].id,
                      spimageUrl: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spImageUrl'),
                      clientimageUrl: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('myImageUrl'),
                      clientEmail: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('clientEmail'),
                      spcontactnumber: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spContactNumber'),
                      clientcontactnumber: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('myContactNumber'),
                      serviceNeeded: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('serviceNeeded'),
                      currentStatus: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('bookingStatus'),
                      deleteStatus: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('deleteStatus'),
                      clientsHere: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('clientsHere'),
                      spsHere: (snapshot.data! as QuerySnapshot)
                          .docs[index]
                          .get('spsHere'),
                    );
                  })
              : Container();
        });
  }

  @override
  void initState() {
    getUserInfo();

    super.initState();
  }

  getUserInfo() async {
    Constants.myName = (await HelperFunctions.getUserNameSharedPreference())!;
    Constants.myEmail = (await HelperFunctions.getUserEmailSharedPreference())!;

    bookDatabase.getBooking(Constants.myEmail).then((value) {
      setState(() {
        bookingStream = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
        backgroundColor: Colors.orange[100],
      ),
      body: bookingList(),
    );
  }
}

class BookingTile extends StatelessWidget {
  final String spName;
  final String spEmail;
  final String clientName;
  final String bookingId;
  final String spimageUrl;
  final String clientimageUrl;
  final String clientEmail;
  final String spcontactnumber;
  final String clientcontactnumber;
  final String serviceNeeded;
  final String currentStatus;
  final String deleteStatus;
  final String clientsHere;
  final String spsHere;
  BookingTile(
      {required this.spName,
      required this.spEmail,
      required this.clientName,
      required this.bookingId,
      required this.spimageUrl,
      required this.clientimageUrl,
      required this.clientEmail,
      required this.spcontactnumber,
      required this.clientcontactnumber,
      required this.serviceNeeded,
      required this.currentStatus,
      required this.deleteStatus,
      required this.clientsHere,
      required this.spsHere});

  BookDatabase bookDatabase = new BookDatabase();

  @override
  Widget build(BuildContext context) {
    deleteBookingInfo(String bookingID) {
      bookDatabase.deleteBookingInfo(bookingID);
    }

    String serviceButton() {
      if (currentStatus == 'Pending') {
        return 'Accept';
      } else if (currentStatus == 'Accepted by Service Provider') {
        return 'Finished';
      } else if (currentStatus == 'Finished') {
        return 'Done';
      } else {
        return '';
      }
    }

    String clientButton() {
      if (currentStatus == 'Pending') {
        return 'Update';
      } else if (currentStatus == 'Accepted by Service Provider') {
        return 'Finished';
      } else if (currentStatus == 'Finished') {
        return 'Done';
      } else {
        return '';
      }
    }

    String cancelDelButton() {
      if (currentStatus == 'Finished') {
        return 'Delete';
      } else {
        return 'Cancel';
      }
    }

    Widget containerFirst() {
      return Container(
        height: 115,
        decoration: BoxDecoration(
          color: Colors.orange[200],
          borderRadius: BorderRadius.circular(5.0),
        ),
        width: (MediaQuery.of(context).size.width) -
            (MediaQuery.of(context).size.width * 0.74),
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: GestureDetector(
            onTap: () async {
              await bookDatabase.getbookingInfo(bookingId);
              Timer(Duration(seconds: 1), () {
                if (spEmail == Constants.myEmail) {
                  if (deleteStatus == 'false') {
                    // await bookDatabase.getbookingInfo(bookingId);

                    deleteBookingSpInstance();
                    createHistorySpNotif();
                  } else if (deleteStatus == 'true' && clientsHere == 'false') {
                    // await bookDatabase.getbookingInfo(bookingId);

                    createHistoryAllNotif();
                    deleteBookingInfo(bookingId);
                  } else if (deleteStatus == 'true' && clientsHere == 'true') {
                    // await bookDatabase.getbookingInfo(bookingId);

                    createHistoryFixNotif(BookingData.spEmail);
                    deleteBookingInfo(bookingId);
                  }
                } else {
                  if (deleteStatus == 'false') {
                    // await bookDatabase.getbookingInfo(bookingId);

                    deleteBookingClientInstance();
                    createHistoryClientNotif();
                  } else if (deleteStatus == 'true' && spsHere == 'false') {
                    createHistoryAllNotif();
                    deleteBookingInfo(bookingId);
                  } else if (deleteStatus == 'true' && spsHere == 'true') {
                    createHistoryFixNotif(BookingData.clientEmail);
                    deleteBookingInfo(bookingId);
                  }
                }
              });
            },
            child: Text(
              'Delete',
              style: TextStyle(fontSize: 16.0),
            ),
          ),
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: ((MediaQuery.of(context).size.width) -
                    (MediaQuery.of(context).size.width * 0.3)),
                height: 115,
                decoration: BoxDecoration(
                  color: Colors.orange[200],
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(60.0),
                      child: CachedNetworkImage(
                        imageUrl: spEmail == Constants.myEmail
                            ? clientimageUrl
                            : spimageUrl,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: 75.0,
                        width: 75.0,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      height: 115,
                      width: ((MediaQuery.of(context).size.width) -
                          (MediaQuery.of(context).size.width * 0.70)),
                      // color: Colors.brown,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            // color: Colors.white,
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            height: 27,
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                spEmail == Constants.myEmail
                                    ? clientName
                                    : spName,
                                style: TextStyle(
                                    fontSize: 16.0, fontFamily: "Brand-Bold"),
                              ),
                            ),
                          ),
                          Container(
                            // color: Colors.white,
                            height: 27,
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                spEmail == Constants.myEmail
                                    ? clientcontactnumber
                                    : spcontactnumber,
                                style: TextStyle(
                                    fontSize: 16.0, fontFamily: "Brand-Bold"),
                              ),
                            ),
                          ),
                          Container(
                            // color: Colors.white,
                            height: 27,
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                'Status:  ' + currentStatus,
                                style: TextStyle(
                                    fontSize: 12.0, fontFamily: "Brand-Bold"),
                              ),
                            ),
                          ),
                          Container(
                            height: 30,
                            color: Colors.blue[50],
                            width: ((MediaQuery.of(context).size.width) -
                                (MediaQuery.of(context).size.width * 0.70)),
                            child: GestureDetector(
                              onTap: () async {
                                await bookDatabase.getbookingInfo(bookingId);

                                Timer(Duration(seconds: 1), () {
                                  Navigator.pushNamed(
                                      context, BookingInfo.idScreen);
                                });
                              },
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Center(
                                  child: Text(
                                    'Click here for more Info...',
                                    style: TextStyle(
                                        fontSize: 10.0,
                                        fontFamily: "Brand-Bold",
                                        color: Colors.blue),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 2,
              ),
              new Container(
                width: (MediaQuery.of(context).size.width) -
                    (MediaQuery.of(context).size.width * 0.74),
                height: 115,
                child: currentStatus == 'Pending' ||
                        currentStatus == 'Accepted by Service Provider' ||
                        currentStatus == 'Finished'
                    ? Column(children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.orange[200],
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          width: (MediaQuery.of(context).size.width) -
                              (MediaQuery.of(context).size.width * 0.74),
                          height: 57,
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: GestureDetector(
                              onTap: () async {
                                await bookDatabase.getbookingInfo(bookingId);
                                Timer(Duration(seconds: 1), () {
                                  if (spEmail == Constants.myEmail) {
                                    if (currentStatus == 'Pending') {
                                      BookingData.status =
                                          'Accepted by Service Provider';
                                      updateBookingInstance();
                                    } else if (currentStatus ==
                                        'Accepted by Service Provider') {
                                      BookingData.status = 'Finished';
                                      updateBookingInstance();
                                    } else {}
                                  } else {
                                    if (currentStatus == 'Pending') {
                                      Navigator.pushNamed(
                                          context, UpdateBooking.idScreen);
                                    } else if (currentStatus ==
                                        'Accepted by Service Provider') {
                                      BookingData.status = 'Finished';
                                      updateBookingInstance();
                                    } else {}
                                  }
                                });
                              },
                              child: Text(
                                spEmail == Constants.myEmail
                                    ? serviceButton()
                                    : clientButton(),
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 1.0,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.orange[200],
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          width: (MediaQuery.of(context).size.width) -
                              (MediaQuery.of(context).size.width * 0.74),
                          height: 57,
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: GestureDetector(
                              onTap: () async {
                                await bookDatabase.getbookingInfo(bookingId);
                                Timer(Duration(seconds: 1), () {
                                  if (spEmail == Constants.myEmail) {
                                    if (currentStatus == 'Pending') {
                                      // await bookDatabase.getbookingInfo(bookingId);

                                      BookingData.status =
                                          'Cancelled by Service Provider';

                                      updateBookingInstance();
                                    } else if (cancelDelButton() == 'Delete') {
                                      if (deleteStatus == 'false') {
                                        // await bookDatabase.getbookingInfo(bookingId);

                                        deleteBookingSpInstance();
                                        createHistorySpNotif();
                                      } else if (deleteStatus == 'true' &&
                                          clientsHere == 'false') {
                                        // await bookDatabase.getbookingInfo(bookingId);

                                        createHistoryAllNotif();
                                        deleteBookingInfo(bookingId);
                                      } else if (deleteStatus == 'true' &&
                                          clientsHere == 'true') {
                                        // await bookDatabase.getbookingInfo(bookingId);

                                        createHistoryFixNotif(
                                            BookingData.spEmail);
                                        deleteBookingInfo(bookingId);
                                      }
                                    } else if (currentStatus ==
                                        'Accepted by Service Provider') {
                                      displayToastMessage(
                                          "You cannot cancel the Appointment because it has been Accepted, Please request Client to Cancel Appointment",
                                          context);
                                    }
                                  } else {
                                    if (cancelDelButton() == 'Cancel') {
                                      //  await bookDatabase.getbookingInfo(bookingId);
                                      BookingData.status =
                                          'Cancelled by Client';
                                      // Timer(Duration(seconds: 1), () {});
                                      updateBookingInstance();
                                    } else if (cancelDelButton() == 'Delete') {
                                      if (deleteStatus == 'false') {
                                        // await bookDatabase.getbookingInfo(bookingId);

                                        deleteBookingClientInstance();
                                        createHistoryClientNotif();
                                      } else if (deleteStatus == 'true' &&
                                          spsHere == 'false') {
                                        // await bookDatabase.getbookingInfo(bookingId);

                                        createHistoryAllNotif();
                                        deleteBookingInfo(bookingId);
                                      } else if (deleteStatus == 'true' &&
                                          spsHere == 'true') {
                                        // await bookDatabase.getbookingInfo(bookingId);

                                        createHistoryFixNotif(
                                            BookingData.clientEmail);
                                        deleteBookingInfo(bookingId);
                                      }
                                    }
                                  }
                                });
                              },
                              child: Text(
                                cancelDelButton(),
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                          ),
                        ),
                      ])
                    : containerFirst(),
              ),
            ]),
      ),
    );
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_LONG);
}

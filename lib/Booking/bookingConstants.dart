class BookingData {
  static String spName = '';
  static String spAddress = '';
  static String spContactNumber = '';
  static String spEmail = '';
  static String clientName = '';
  static String clientAddress = '';
  static String clientContactNumber = '';
  static String clientEmail = '';
  static String serviceNeeded = '';
  static String status = '';
  static String bookingDateTime = '';
  static String spImageUrl = '';
  static String clientImageUrl = '';
  //static String bookingId = '';
  static String scheduleBooking = '';
  static String schedHour = '';
  static String schedMinute = '';
  static String schedAmPm = '';
  static dynamic dateform = '';
  static String bookingIdentification = '';
}

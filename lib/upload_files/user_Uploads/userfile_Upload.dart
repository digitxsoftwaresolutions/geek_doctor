import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geek_doctor/AllWidgets/button_Widget.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/home/user_home/userInfoTile.dart';
import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/models/user_data/user.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:geek_doctor/upload_files/user_Uploads/userStorageDatabase.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';

class UserFileUpload extends StatefulWidget {
  static const String idScreen = 'UserFileUpload';

  @override
  _UserFileUploadState createState() => _UserFileUploadState();
}

class _UserFileUploadState extends State<UserFileUpload> {
  UploadTask? task;
  File? file;
  String downloadUrl = '';
  final picker = ImagePicker();
  @override
  Widget build(BuildContext context) {
    final fileName = file != null ? basename(file!.path) : 'No File Selected';
    final user = Provider.of<Usercount?>(context);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user!.uid).userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData? userData = snapshot.data;
            return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: Text('Upload Picture'),
                backgroundColor: Colors.orange[100],
              ),
              body: Container(
                padding: EdgeInsets.all(32),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ButtonWidget(
                          text: 'Select Image',
                          icon: Icons.attach_file,
                          onClicked: () async {
                            final pickedFile = await picker.pickImage(
                                source: ImageSource.gallery);

                            if (pickedFile == null) return;
                            final path = pickedFile.path;

                            setState(() => file = File(path));
                          }),
                      SizedBox(height: 8),
                      Text(
                        fileName,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 48),
                      SizedBox(height: 10),
                      ButtonWidget(
                          text: 'Upload File',
                          icon: Icons.cloud_upload_outlined,
                          onClicked: () async {
                            if (file == null) return;

                            final fileName = basename(file!.path);
                            final destination = 'User/images/$fileName';

                            task = FirebaseApi.uploadFile(destination, file!);
                            setState(() {});

                            if (task == null) return;

                            final snapshot = await task!.whenComplete(() {});
                            final urlDownload =
                                await snapshot.ref.getDownloadURL();
                            downloadUrl = urlDownload;
                            //print('Download-Link: $downloadUrl');

                            await DatabaseService(uid: user.uid).updateUserData(
                                userData!.name,
                                userData.lastname,
                                userData.birthdate,
                                userData.gender,
                                userData.address,
                                userData.email,
                                userData.contact,
                                downloadUrl,
                                userData.position);
                            Navigator.pop(context);
                          }),
                      SizedBox(height: 20),
                      task != null ? buildUploadStatus(task!) : Container(),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Loading();
          }
        });
  }

  Widget buildUploadStatus(UploadTask task) => StreamBuilder<TaskSnapshot>(
        stream: task.snapshotEvents,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final snap = snapshot.data!;
            final progress = snap.bytesTransferred / snap.totalBytes;
            final percentage = (progress * 100).toStringAsFixed(2);

            return Text(
              '$percentage %',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            );
          } else {
            return Container();
          }
        },
      );
}
